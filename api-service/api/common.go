// Package api is used to create routing and handler functions providing an API
// gateway for interacting with Nafigos services over HTTP
package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/cyverse/nafigos/common"
)

// jsonError will create and write a nice JSON error response
func jsonError(w http.ResponseWriter, msg string, code int) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write([]byte(fmt.Sprintf(`{"error": {"code": %d, "message": "%s"}}`, code, msg)))
}

func idResponse(w http.ResponseWriter, id string, code int) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write([]byte(fmt.Sprintf(`{"id": "%s"}`, id)))
}

func getResponseCode(data []byte) int {
	var response struct {
		Error struct {
			Code int `json:"code"`
		} `json:"error"`
	}
	json.Unmarshal(data, &response)
	if response.Error.Code == 0 {
		return http.StatusOK
	}
	return response.Error.Code
}

func getWorkflowDefinitionData(request common.Request) (int, error) {
	// Synchronously request WorkflowDefinition
	msg, err := common.PublishRequest(
		request,
		"WorkflowDefinition.Get",
		"API",
		natsInfo["address"],
	)
	if err != nil {
		return http.StatusServiceUnavailable, err
	}

	err = json.Unmarshal(msg, request)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	if len(request.GetError().Message) > 0 {
		return request.GetError().Code, fmt.Errorf(request.GetError().Message)
	}

	return http.StatusOK, nil
}

func prepareRequest(request common.Request, r *http.Request, workflowID string) (status int, err error) {
	if r.Body != nil {
		var reqBody []byte
		reqBody, err = ioutil.ReadAll(r.Body)
		if err != nil {
			status = http.StatusInternalServerError
			return
		}
		if len(reqBody) > 0 {
			err = json.Unmarshal(reqBody, &request)
			if err != nil {
				err = fmt.Errorf("Error reading request body: " + err.Error())
				status = http.StatusBadRequest
				return
			}
		}
	}
	if len(request.GetWorkflowID()) == 0 {
		request.SetWorkflowID(workflowID)
	}
	user := request.GetUser()
	user.Username = r.Header.Get("X-Nafigos-User")
	r.Header.Del("X-Nafigos-User")

	vaultToken := r.Header.Get("X-Vault-Token")
	r.Header.Del("X-Vault-Token")
	// Get user's secret if SecretID included in the request
	user.Secrets = map[string]common.Secret{}

	if len(request.GetRegistrySecretID()) > 0 {
		var secret common.Secret
		secret, status, err = vaultClient.ReadSecret(
			user.Username,
			request.GetRegistrySecretID(),
			vaultToken,
		)
		if err != nil {
			err = fmt.Errorf("Error reading container registry secret '%s': %s", request.GetRegistrySecretID(), err.Error())
			return
		}
		user.Secrets[request.GetRegistrySecretID()] = secret
	}
	if len(request.GetGitSecretID()) > 0 {
		var secret common.Secret
		secret, status, err = vaultClient.ReadSecret(
			user.Username,
			request.GetGitSecretID(),
			vaultToken,
		)
		if err != nil {
			err = fmt.Errorf("Error reading git secret '%s': %s", request.GetGitSecretID(), err.Error())
			return
		}
		user.Secrets[request.GetGitSecretID()] = secret
	}
	// Get user's cluster if ClusterID included in the request
	if len(request.GetClusterID()) > 0 {
		var cluster common.Cluster
		cluster, status, err = vaultClient.ReadCluster(
			user.Username,
			request.GetClusterID(),
			vaultToken,
		)
		if err != nil {
			err = fmt.Errorf("Error reading cluster '%s': %s", request.GetClusterID(), err.Error())
			return
		}
		user.Clusters = map[string]common.Cluster{}
		user.Clusters[request.GetClusterID()] = cluster
	}
	request.SetUser(user)
	status = http.StatusOK
	return
}

func checkAuthorization(request common.Request, r *http.Request, workflowID string) (int, error) {
	status, err := prepareRequest(request, r, workflowID)
	if err != nil {
		return status, err
	}
	status, err = getWorkflowDefinitionData(request)
	if err != nil {
		return status, err
	}
	return http.StatusOK, nil
}
