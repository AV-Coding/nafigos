package api

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func testUpdateUser(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("WriteUser", common.User{
		Username: "test_username",
		Secrets: map[string]common.Secret{
			"1": {
				Type:     "github",
				Username: "other_githubusername",
				Value:    "other_githubtoken",
			},
		},
	}, "vault_token").Return(200, nil)
	vaultClient.GetMock().On("CreateNewPolicy", "test_username", false).
		Return(200, nil)

	// Create request
	req, err := http.NewRequest("PUT", "/users/test_username", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"secrets": {
		"1": {
			"type": "github",
			"username": "other_githubusername",
			"value": "other_githubtoken"
		}
	}
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}", updateUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusCreated, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testUpdateUserAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("PUT", "/users/test_username", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}", updateUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testUpdateUserBadRequest(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("PUT", "/users/test_username", nil)
	assert.NoError(t, err)
	// Bad JSON request body
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}", updateUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "unable to parse request into user: unexpected end of JSON input"}}`, rr.Body.String())
}

func testUpdateUserPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On("WriteUser", common.User{
		Username: "test_username",
		Secrets: map[string]common.Secret{
			"1": {
				Type:     "github",
				Username: "other_githubusername",
				Value:    "other_githubtoken",
			},
		},
	}, "vault_token").Return(403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("PUT", "/users/test_username", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"secrets": {
		"1": {
			"type": "github",
			"username": "other_githubusername",
			"value": "other_githubtoken"
		}
	}
}`)))
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}", updateUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to write user: permission denied"}}`, rr.Body.String())
}

func testGetUser(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadUser", "test_username", "vault_token").
		Return(common.User{
			Username: "test_username",
			IsAdmin:  false,
			Secrets: map[string]common.Secret{
				"9m4e2mr0ui3e8a215n4g": {
					ID:       "9m4e2mr0ui3e8a215n4g",
					Type:     "github",
					Username: "test_githubusername",
					Value:    "test_githubtoken",
				},
			},
			Clusters: map[string]common.Cluster{},
		}, 200, nil)

	// Create request
	req, err := http.NewRequest("GET", "/users/test_username", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}", getUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.Equal(t, `{"username":"test_username","secrets":{"9m4e2mr0ui3e8a215n4g":{"username":"test_githubusername","value":"test_githubtoken","type":"github","id":"9m4e2mr0ui3e8a215n4g"}},"is_admin":false}`, rr.Body.String())
}

func testGetUserAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("GET", "/users/test_username", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}", getUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testGetUserDNE(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadUser", "fake_username", "vault_token").
		Return(common.User{}, 404, fmt.Errorf("user 'fake_username' not found"))

	// Create request
	req, err := http.NewRequest("GET", "/users/fake_username", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}", getUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusNotFound, rr.Code)
	assert.Equal(t, `{"error": {"code": 404, "message": "Error getting User: user 'fake_username' not found"}}`, rr.Body.String())
}

func testGetUserPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadUser", "test_username", "vault_token").
		Return(common.User{}, 403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("GET", "/users/test_username", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}", getUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to get user: permission denied"}}`, rr.Body.String())
}

func testGetUsers(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadUser", "test_username", "vault_token").
		Return(common.User{
			Username: "test_username",
			IsAdmin:  true,
		}, 200, nil)
	vaultClient.GetMock().On("ListAllUsers").
		Return([]string{"user1", "user2"}, 200, nil)

	req, err := http.NewRequest("GET", "/users", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users", getUsers)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Contains(t, []string{
		`["user1","user2"]`,
		`["user2","user1"]`,
	}, rr.Body.String())
}

func testGetUsersError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadUser", "test_username", "vault_token").
		Return(common.User{
			Username: "test_username",
			IsAdmin:  true,
		}, 200, nil)
	vaultClient.GetMock().On("ListAllUsers").
		Return([]string{}, 500, fmt.Errorf("mock error"))

	req, err := http.NewRequest("GET", "/users", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users", getUsers)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to get list of users: mock error"}}`, rr.Body.String())
}

func testGetUsersAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("GET", "/users", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users", getUsers)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testGetAllSecrets(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("GetAllSecrets", "test_username", "vault_token").
		Return(map[string]common.Secret{
			"9m4e2mr0ui3e8a215n4g": {
				ID:       "9m4e2mr0ui3e8a215n4g",
				Type:     "github",
				Username: "test_githubusername",
				Value:    "test_githubtoken",
			},
		}, 200, nil)

	// Create request
	req, err := http.NewRequest("GET", "/users/test_username/secrets", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets", getAllSecrets)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.Equal(t, `{"9m4e2mr0ui3e8a215n4g":{"username":"test_githubusername","value":"test_githubtoken","type":"github","id":"9m4e2mr0ui3e8a215n4g"}}`, rr.Body.String())
}

func testGetAllSecretsAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("GET", "/users/test_username/secrets", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets", getAllSecrets)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testGetAllSecretsPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On("GetAllSecrets", "test_username", "vault_token").
		Return(map[string]common.Secret{}, 403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("GET", "/users/test_username/secrets", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets", getAllSecrets)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to get secrets: permission denied"}}`, rr.Body.String())
}

func testAddNewSecret(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("AddSecret", "test_username", mock.Anything, map[string]interface{}{
		"type":     "github",
		"username": "other_githubusername",
		"value":    "other_githubtoken",
	}, "vault_token").Return(200, nil)

	// Create request
	req, err := http.NewRequest("POST", "/users/test_username/secrets", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"type": "github",
	"username": "other_githubusername",
	"value": "other_githubtoken"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets", addNewSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusCreated, rr.Code)
	assert.Regexp(t, `{"id":"[a-z0-9]{20}","type":"github","username":"other_githubusername","value":"other_githubtoken"}`, rr.Body.String())
}

func testAddNewSecretAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("POST", "/users/test_username/secrets", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets", addNewSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testAddNewSecretBadRequest(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("POST", "/users/test_username/secrets", nil)
	assert.NoError(t, err)
	// Bad JSON request body
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets", addNewSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "unable to parse request into secret: unexpected end of JSON input"}}`, rr.Body.String())
}

func testAddNewSecretPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On("AddSecret", "test_username", mock.Anything, map[string]interface{}{
		"type":     "github",
		"username": "other_githubusername",
		"value":    "other_githubtoken",
	}, "vault_token").Return(403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("POST", "/users/test_username/secrets", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"type": "github",
	"username": "other_githubusername",
	"value": "other_githubtoken"
}`)))
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets", addNewSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to write secret: permission denied"}}`, rr.Body.String())
}

func testGetSecret(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{
			ID:       "9m4e2mr0ui3e8a215n4g",
			Type:     "github",
			Username: "test_githubusername",
			Value:    "test_githubtoken",
		}, 200, nil)

	// Create request
	req, err := http.NewRequest("GET", "/users/test_username/secrets/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets/{secretID}", getSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.Equal(t, `{"username":"test_githubusername","value":"test_githubtoken","type":"github","id":"9m4e2mr0ui3e8a215n4g"}`, rr.Body.String())
}

func testGetSecretAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("GET", "/users/test_username/secrets/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets/{secretID}", getSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testGetSecretPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{}, 403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("GET", "/users/test_username/secrets/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets/{secretID}", getSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to get secret: permission denied"}}`, rr.Body.String())
}

func testEditSecret(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On(
		"EditSecret",
		"test_username",
		"9m4e2mr0ui3e8a215n4g",
		common.Secret{Value: "new_value"},
		"vault_token",
	).Return(200, nil)

	// Create request
	req, err := http.NewRequest("PUT", "/users/test_username/secrets/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{"value": "new_value"}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets/{secretID}", editSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusCreated, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testEditSecretAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("PUT", "/users/test_username/secrets/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets/{secretID}", editSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testEditSecretBadRequest(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("PUT", "/users/test_username/secrets/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	// Bad JSON request body
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets/{secretID}", editSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "unable to parse request into secret: unexpected end of JSON input"}}`, rr.Body.String())
}

func testEditSecretPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On(
		"EditSecret",
		"test_username",
		"9m4e2mr0ui3e8a215n4g",
		common.Secret{Value: "new_value"},
		"vault_token",
	).Return(403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("PUT", "/users/test_username/secrets/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{"value": "new_value"}`)))
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets/{secretID}", editSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to edit secret: permission denied"}}`, rr.Body.String())
}

func testDeleteSecret(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("DeleteSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(200, nil)

	// Create request
	req, err := http.NewRequest("DELETE", "/users/test_username/secrets/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets/{secretID}", deleteSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusNoContent, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testDeleteSecretAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("DELETE", "/users/test_username/secrets/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets/{secretID}", deleteSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testDeleteSecretPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On("DeleteSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("DELETE", "/users/test_username/secrets/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/users/{username}/secrets/{secretID}", deleteSecret)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to delete secret: permission denied"}}`, rr.Body.String())
}
