package api

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"regexp"
	"sync"
	"testing"
	"time"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/gorilla/mux"
	"github.com/nats-io/nats.go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	core "k8s.io/api/core/v1"
)

func testStartRun(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(2)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				WorkflowID string `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.WorkflowID)
			msg.Respond([]byte(`{"workflow_definition": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	go common.StreamingTestSubscriber(
		"Pleo.Run",
		"Pleo.RunQueue",
		func(reqBytes []byte, natsInfo map[string]string) {
			var request struct {
				User common.User `json:"user"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, common.Cluster{
				ID:               "9m4e2mr0ui3e8a216o5h",
				Name:             "default",
				DefaultNamespace: "default",
				Config:           "config",
			}, request.User.Clusters["9m4e2mr0ui3e8a216o5h"])
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "pleo",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("POST", "/runs", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"workflow_id": "9m4e2mr0ui3e8a215n4g",
	"cluster_id": "9m4e2mr0ui3e8a216o5h"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs", startRun)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusAccepted, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Regexp(t, regexp.MustCompile(`{"id": "[a-z0-9]{20}"}`), rr.Body.String())
}

func testStartRunExtraFlags(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(2)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				WorkflowID string `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.WorkflowID)
			msg.Respond([]byte(`{"workflow_definition": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	go common.StreamingTestSubscriber(
		"Pleo.Run",
		"Pleo.RunQueue",
		func(reqBytes []byte, natsInfo map[string]string) {
			var request struct {
				User         common.User        `json:"user"`
				TCPPorts     []core.ServicePort `json:"tcp_ports"`
				HTTPPorts    []core.ServicePort `json:"http_ports"`
				AuthDisabled bool               `json:"auth_disabled"`
				Namespace    string             `json:"namespace"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, common.Cluster{
				ID:               "9m4e2mr0ui3e8a216o5h",
				Name:             "default",
				DefaultNamespace: "default",
				Config:           "config",
			}, request.User.Clusters["9m4e2mr0ui3e8a216o5h"])
			assert.Equal(t, true, request.AuthDisabled)
			assert.Equal(t, "test-namespace", request.Namespace)
			assert.Equal(t, []core.ServicePort{{Name: "test", Port: int32(10000)}}, request.HTTPPorts)
			assert.Equal(t, []core.ServicePort{{Name: "test", Port: int32(10001)}}, request.TCPPorts)
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "pleo",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("POST", "/runs", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"workflow_id": "9m4e2mr0ui3e8a215n4g",
	"cluster_id": "9m4e2mr0ui3e8a216o5h",
	"auth_disabled": true,
	"namespace": "test-namespace",
	"http_ports": [{"name": "test", "port": 10000}],
	"tcp_ports": [{"name": "test", "port": 10001}]
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs", startRun)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusAccepted, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Regexp(t, regexp.MustCompile(`{"id": "[a-z0-9]{20}"}`), rr.Body.String())
}

func testStartRunBadRequest(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	req, err := http.NewRequest("POST", "/runs", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs", startRun)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading request body: unexpected end of JSON input"}}`, rr.Body.String())
}

func testStartRunNoClusterID(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	req, err := http.NewRequest("POST", "/runs", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
		"workflow_id": "9m4e2mr0ui3e8a215n4g"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs", startRun)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "required cluster ID was not provided"}}`, rr.Body.String())
}

func testStartRunNoWorkflowID(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	req, err := http.NewRequest("POST", "/runs", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
		"cluster_id": "9m4e2mr0ui3e8a216o5h"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs", startRun)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "required workflow ID was not provided"}}`, rr.Body.String())
}

func testStartRunGetWorkflowError(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				WorkflowID string `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a217p6i", request.WorkflowID)
			msg.Respond([]byte(`{"error": {"code": 500, "message": "mock error"}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("POST", "/runs", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"workflow_id": "9m4e2mr0ui3e8a217p6i",
	"cluster_id": "9m4e2mr0ui3e8a216o5h"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs", startRun)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to get WorkflowDefinition data: mock error"}}`, rr.Body.String())
}

func testStartRunBadWorkflow(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				WorkflowID string `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.WorkflowID)
			msg.Respond([]byte(`{"data": {"error_msg": "Error"}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("POST", "/runs", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"workflow_id": "9m4e2mr0ui3e8a215n4g",
	"cluster_id": "9m4e2mr0ui3e8a216o5h"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs", startRun)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "Unable to run WorkflowDefinition in an error state. Please fix Workflow and try again"}}`, rr.Body.String())
}

func testGetRun(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"Phylax.GetRunner",
		"Phylax.GetRunnerQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User  common.User `json:"user"`
				RunID string      `json:"run_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "run_id", request.RunID)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"run": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/runs/run_id", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs/{runID}", getRun)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"run": {}}`, rr.Body.String())
}

func testGetRunBadRequest(t *testing.T) {
	// Create request
	req, err := http.NewRequest("GET", "/runs/run_id", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs/{runID}", getRun)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading request body: unexpected end of JSON input"}}`, rr.Body.String())
}

func testGetRunError(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"Phylax.GetRunner",
		"Phylax.GetRunnerQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User  common.User `json:"user"`
				RunID string      `json:"run_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "run_id", request.RunID)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"error": {"code": 500, "message": "mock error"}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/runs/run_id", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs/{runID}", getRun)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "mock error"}}`, rr.Body.String())
}

func testGetRunNATSError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("GET", "/runs/run_id", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs/{runID}", getRun)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to publish request to get run: error connecting to NATS: nats: no servers available for connection"}}`, rr.Body.String())
}

func testGetRunsForUser(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"Phylax.GetRunnersForUser",
		"Phylax.GetRunnersForUserQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User common.User `json:"user"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"runs": []}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/runs", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs", getRunsForUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"runs": []}`, rr.Body.String())
}

func testGetRunsForUserBadRequest(t *testing.T) {
	// Create request
	req, err := http.NewRequest("GET", "/runs", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs", getRunsForUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading request body: unexpected end of JSON input"}}`, rr.Body.String())
}

func testGetRunsForUserError(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"Phylax.GetRunnersForUser",
		"Phylax.GetRunnersForUserQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User common.User `json:"user"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"error": {"code": 500, "message": "mock error"}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/runs", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs", getRunsForUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "mock error"}}`, rr.Body.String())
}

func testGetRunsForUserNATSError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("GET", "/runs", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs", getRunsForUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to publish request to get runs: error connecting to NATS: nats: no servers available for connection"}}`, rr.Body.String())
}

func testUpdateRun(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("PUT", "/runs/9n5f4ns0ui3e8a217p6i", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs/{runID}", updateRun)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusNotImplemented, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 501, "message": "feature not implemented"}}`, rr.Body.String())
}

func testDeleteRun(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"Phylax.DeleteRun",
		"Phylax.DeleteRunQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User common.User `json:"user"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(""))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)

	// Create request
	req, err := http.NewRequest("DELETE", "/runs/9n5f4ns0ui3e8a217p6i", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs/{runID}", deleteRun)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusNoContent, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testDeleteRunBadRequest(t *testing.T) {
	// Create request
	req, err := http.NewRequest("DELETE", "/runs/9n5f4ns0ui3e8a217p6i", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/runs/{runID}", deleteRun)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading request body: unexpected end of JSON input"}}`, rr.Body.String())
}

func testScaleFromZero(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(3)
	go common.StreamingTestSubscriber(
		"Pleo.ScaleUp",
		"Pleo.ScaleUpQueue",
		func(reqBytes []byte, natsInfo map[string]string) {
			var request struct {
				User common.User `json:"user"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, common.Cluster{
				ID: "clusterID",
			}, request.User.Clusters["clusterID"])
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "pleo",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	go common.SynchronousTestSubscriber(
		"Phylax.GetRunner",
		"Phylax.GetRunnerQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User  common.User `json:"user"`
				RunID string      `json:"run_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9n5f4ns0ui3e8a217p6i", request.RunID)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{
"runner": {
	"workflowdefinitionid": "9m4e2mr0ui3e8a215n4g",
	"cluster": {"id": "clusterID"},
	"namespace": "testNamespace",
	"resourcename": "my-resource"
}, "error": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "pleo",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User       common.User `json:"user"`
				WorkflowID string      `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.WorkflowID)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"workflow_definition": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "pleo",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	maxRetry = 0
	waitTimeSeconds = 0
	// Create request
	req, err := http.NewRequest("GET", "/9n5f4ns0ui3e8a217p6i", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/{runID}", scaleFromZero)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testScaleFromZeroBadRequest(t *testing.T) {
	// Create request
	req, err := http.NewRequest("GET", "/9n5f4ns0ui3e8a217p6i", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/{runID}", scaleFromZero)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading request body: unexpected end of JSON input"}}`, rr.Body.String())
}
