package api

import (
	"gitlab.com/cyverse/nafigos/common"

	"github.com/stretchr/testify/mock"
)

// MockVaultClient ...
type MockVaultClient struct {
	Mock       *mock.Mock
	AuthMethod string
}

// GetAuthMethod ...
func (client *MockVaultClient) GetAuthMethod() string {
	return client.AuthMethod
}

// GetMock ...
func (client *MockVaultClient) GetMock() *mock.Mock {
	return client.Mock
}

// AddSecret ...
func (client *MockVaultClient) AddSecret(path string, id string, secret map[string]interface{}, token string) (int, error) {
	args := client.GetMock().Called(path, id, secret, token)
	return args.Int(0), args.Error(1)
}

// EditSecret ...
func (client *MockVaultClient) EditSecret(owner string, id string, secret common.Secret, token string) (int, error) {
	args := client.GetMock().Called(owner, id, secret, token)
	return args.Int(0), args.Error(1)
}

// DeleteSecret ...
func (client *MockVaultClient) DeleteSecret(owner, id, token string) (int, error) {
	args := client.GetMock().Called(owner, id, token)
	return args.Int(0), args.Error(1)
}

// ReadSecret ...
func (client *MockVaultClient) ReadSecret(owner, id, token string) (common.Secret, int, error) {
	args := client.GetMock().Called(owner, id, token)
	return args.Get(0).(common.Secret), args.Int(1), args.Error(2)
}

// GetAllSecrets ...
func (client *MockVaultClient) GetAllSecrets(owner, token string) (secrets map[string]common.Secret, code int, err error) {
	args := client.GetMock().Called(owner, token)
	return args.Get(0).(map[string]common.Secret), args.Int(1), args.Error(2)
}

// GetUserLoginToken ...
func (client *MockVaultClient) GetUserLoginToken(username, token string) (string, error) {
	args := client.GetMock().Called(username, token)
	return args.String(0), args.Error(1)
}

// WriteUser ...
func (client *MockVaultClient) WriteUser(user common.User, token string) (int, error) {
	args := client.GetMock().Called(user, token)
	return args.Int(0), args.Error(1)
}

// DeleteUser ...
func (client *MockVaultClient) DeleteUser(username string) (int, error) {
	args := client.GetMock().Called(username)
	return args.Int(0), args.Error(1)
}

// ReadUser ...
func (client *MockVaultClient) ReadUser(owner, token string) (user common.User, code int, err error) {
	args := client.GetMock().Called(owner, token)
	return args.Get(0).(common.User), args.Int(1), args.Error(2)
}

// ListAllUsers ...
func (client *MockVaultClient) ListAllUsers() (users []string, code int, err error) {
	args := client.GetMock().Called()
	return args.Get(0).([]string), args.Int(1), args.Error(2)
}

// AddCluster ...
func (client *MockVaultClient) AddCluster(owner string, id string, cluster common.Cluster, token string) (int, error) {
	args := client.GetMock().Called(owner, id, cluster, token)
	return args.Int(0), args.Error(1)
}

// ReadCluster ...
func (client *MockVaultClient) ReadCluster(owner, id, token string) (common.Cluster, int, error) {
	args := client.GetMock().Called(owner, id, token)
	return args.Get(0).(common.Cluster), args.Int(1), args.Error(2)
}

// EditCluster ...
func (client *MockVaultClient) EditCluster(owner string, id string, newCluster common.Cluster, token string) (int, error) {
	args := client.GetMock().Called(owner, id, newCluster, token)
	return args.Int(0), args.Error(1)
}

// DeleteCluster ...
func (client *MockVaultClient) DeleteCluster(owner, id, token string) (int, error) {
	args := client.GetMock().Called(owner, id, token)
	return args.Int(0), args.Error(1)
}

// GetAllClusters ...
func (client *MockVaultClient) GetAllClusters(owner, token string) (clusters map[string]common.Cluster, code int, err error) {
	args := client.GetMock().Called(owner, token)
	return args.Get(0).(map[string]common.Cluster), args.Int(1), args.Error(2)
}

// CreateNewPolicy ...
func (client *MockVaultClient) CreateNewPolicy(owner string, isAdmin bool) (int, error) {
	args := client.GetMock().Called(owner, isAdmin)
	return args.Int(0), args.Error(1)
}

// CreateAdminPolicy ...
func (client *MockVaultClient) CreateAdminPolicy() (int, error) {
	args := client.GetMock().Called()
	return args.Int(0), args.Error(1)
}

// MountNewKVPath ...
func (client *MockVaultClient) MountNewKVPath(owner string) (int, error) {
	args := client.GetMock().Called(owner)
	return args.Int(0), args.Error(1)
}

// EnableJWTAuth ...
func (client *MockVaultClient) EnableJWTAuth(oidcURL string) error {
	args := client.GetMock().Called(oidcURL)
	return args.Error(0)
}

// WriteJWTRole ...
func (client *MockVaultClient) WriteJWTRole(username string, isAdmin bool) (int, error) {
	args := client.GetMock().Called(username, isAdmin)
	return args.Int(0), args.Error(1)
}

// EnableUserpassAuth ...
func (client *MockVaultClient) EnableUserpassAuth() error {
	args := client.GetMock().Called()
	return args.Error(0)
}

// WriteUserpass ...
func (client *MockVaultClient) WriteUserpass(username, password string) (int, error) {
	args := client.GetMock().Called(username, password)
	return args.Int(0), args.Error(1)
}
