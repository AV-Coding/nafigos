package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/gorilla/mux"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
)

// UserAPIRouter creates routes for user-related operations such as creating and
// updating users and logging in
func UserAPIRouter(router *mux.Router) {
	router.HandleFunc("/users", getUsers).Methods("GET")
	router.HandleFunc("/users/{username}", getUser).Methods("GET")
	router.HandleFunc("/users/{username}", updateUser).Methods("PUT")
	router.HandleFunc("/users/{username}/secrets", getAllSecrets).Methods("GET")
	router.HandleFunc("/users/{username}/secrets", addNewSecret).Methods("POST")
	router.HandleFunc("/users/{username}/secrets/{secretID}", getSecret).Methods("GET")
	router.HandleFunc("/users/{username}/secrets/{secretID}", editSecret).Methods("PUT")
	router.HandleFunc("/users/{username}/secrets/{secretID}", deleteSecret).Methods("DELETE")
}

// updateUser allows someone to update a user's information/secrets in Vault
func updateUser(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateUser",
		"username": username,
	})
	logger.Info("received request to update user")

	_, _, vaultToken := readAuthHeaders(w, r)
	if len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}

	// Get new user info from request
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		jsonError(w, "unable to read request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	var user common.User
	err = json.Unmarshal(reqBody, &user)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse request into user")
		jsonError(w, "unable to parse request into user: "+err.Error(), http.StatusBadRequest)
		return
	}
	user.Username = username

	logUser := user.GetRedacted()
	logger = logger.WithFields(log.Fields{"user": logUser})
	logger.Info("successfully parsed new user")

	// Write user info to Vault
	code, err := vaultClient.WriteUser(user, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to write user")
		jsonError(w, "unable to write user: "+err.Error(), code)
		return
	}
	// Rewrite the user's Policy to make sure the user has correct permissions when IsAdmin is changed
	code, err = vaultClient.CreateNewPolicy(user.Username, user.IsAdmin)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to rewrite user's policy")
		jsonError(w, "unable to rewrite user's policy: "+err.Error(), code)
		return
	}
	if vaultClient.GetAuthMethod() == "keycloak" {
		code, err = vaultClient.WriteJWTRole(user.Username, user.IsAdmin)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create JWT role")
			jsonError(w, "unable to create JWT role: "+err.Error(), code)
			return
		}
	}
	w.WriteHeader(http.StatusCreated)
}

func getUsers(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getUsers",
	})
	logger.Info("received request to get users")

	authedUser, admin, vaultToken := readAuthHeaders(w, r)
	if len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}
	logger = logger.WithFields(log.Fields{"username": authedUser})
	logger.Info("got username for requesting user")

	var resultBytes []byte
	if admin {
		result, code, err := vaultClient.ListAllUsers()
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to get list of users")
			jsonError(w, "unable to get list of users: "+err.Error(), code)
			return
		}
		resultBytes, err = json.Marshal(result)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response")
			jsonError(w, "unable to marshal response: "+err.Error(), http.StatusInternalServerError)
			return
		}
	} else {
		resultBytes = []byte(fmt.Sprintf(`["%s"]`, authedUser))
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resultBytes)
}

func getUser(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getUser",
		"username": username,
	})
	logger.Info("received request to get user")

	authedUser, admin, vaultToken := readAuthHeaders(w, r)
	if len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}
	logger = logger.WithFields(log.Fields{"requesting_user": authedUser})
	logger.Info("got username for requesting user")

	// Make sure authenticated user has permission to get info on this user
	if authedUser != username && !admin {
		err := fmt.Errorf("non-admin user '%s' cannot get other user '%s'", authedUser, username)
		logger.WithFields(log.Fields{"error": err}).Error("unable to get user")
		jsonError(w, fmt.Sprintf("unable to get user: %v", err), http.StatusForbidden)
		return
	}

	// Get user info from Vault
	user, code, err := vaultClient.ReadUser(username, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get user")
		jsonError(w, "unable to get user: "+err.Error(), code)
		return
	}

	logUser := user.GetRedacted()
	logger = logger.WithFields(log.Fields{"user": logUser})
	logger.Info("successfully got user")

	userBytes, err := json.Marshal(user)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response")
		jsonError(w, "unable to marshal response: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(userBytes)
}

func getAllSecrets(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getAllSecrets",
		"username": username,
	})
	logger.Info("received request to get secrets")

	_, _, vaultToken := readAuthHeaders(w, r)
	if len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}

	secrets, code, err := vaultClient.GetAllSecrets(username, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get secrets")
		jsonError(w, "unable to get secrets: "+err.Error(), code)
		return
	}

	var logSecrets []common.Secret
	for _, secret := range secrets {
		logSecrets = append(logSecrets, secret.GetRedacted())
	}
	logger = logger.WithFields(log.Fields{"secrets": logSecrets})
	logger.Info("successfully got all secrets")

	secretsBytes, err := json.Marshal(secrets)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response")
		jsonError(w, "unable to marshal response: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(secretsBytes)
}

func addNewSecret(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	secretID := xid.New().String()
	logger := log.WithFields(log.Fields{
		"package":   "api",
		"function":  "addNewSecret",
		"username":  username,
		"secret_id": secretID,
	})
	logger.Info("received request to add a new secret")

	_, _, vaultToken := readAuthHeaders(w, r)
	if len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}

	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		jsonError(w, "unable to read request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	var secretMap map[string]interface{}
	err = json.Unmarshal(bodyBytes, &secretMap)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse request into secret")
		jsonError(w, "unable to parse request into secret: "+err.Error(), http.StatusBadRequest)
		return
	}

	code, err := vaultClient.AddSecret(username, secretID, secretMap, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to write secret")
		jsonError(w, "unable to write secret: "+err.Error(), code)
		return
	}
	secretMap["id"] = secretID
	secretsBytes, err := json.Marshal(secretMap)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response")
		jsonError(w, "unable to marshal response: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(secretsBytes)
}

func getSecret(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	secretID := mux.Vars(r)["secretID"]
	logger := log.WithFields(log.Fields{
		"package":   "api",
		"function":  "getSecret",
		"username":  username,
		"secret_id": secretID,
	})
	logger.Info("received request to get a secret")

	_, _, vaultToken := readAuthHeaders(w, r)
	if len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}

	secret, code, err := vaultClient.ReadSecret(username, secretID, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get secret")
		jsonError(w, "unable to get secret: "+err.Error(), code)
		return
	}

	logSecret := secret.GetRedacted()
	logger = logger.WithFields(log.Fields{"secret": logSecret})
	logger.Info("successfully got secret")

	secretBytes, err := json.Marshal(secret)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response")
		jsonError(w, "unable to marshal response: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(secretBytes)
}

func editSecret(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	secretID := mux.Vars(r)["secretID"]
	logger := log.WithFields(log.Fields{
		"package":   "api",
		"function":  "editSecret",
		"username":  username,
		"secret_id": secretID,
	})
	logger.Info("received request to edit a secret")

	_, _, vaultToken := readAuthHeaders(w, r)
	if len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}

	// Get new secret params from request body
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		jsonError(w, "unable to read request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	var secret common.Secret
	err = json.Unmarshal(bodyBytes, &secret)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse request into secret")
		jsonError(w, "unable to parse request into secret: "+err.Error(), http.StatusBadRequest)
		return
	}

	logSecret := secret.GetRedacted()
	logger = logger.WithFields(log.Fields{"secret": logSecret})
	logger.Info("successfully parsed new secret")

	code, err := vaultClient.EditSecret(username, secretID, secret, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to edit secret")
		jsonError(w, "unable to edit secret: "+err.Error(), code)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func deleteSecret(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	secretID := mux.Vars(r)["secretID"]
	logger := log.WithFields(log.Fields{
		"package":   "api",
		"function":  "deleteSecret",
		"username":  username,
		"secret_id": secretID,
	})
	logger.Info("received request to delete a secret")

	_, _, vaultToken := readAuthHeaders(w, r)
	if len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}

	code, err := vaultClient.DeleteSecret(username, secretID, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to delete secret")
		jsonError(w, "unable to delete secret: "+err.Error(), code)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func readAuthHeaders(w http.ResponseWriter, r *http.Request) (string, bool, string) {
	username := r.Header.Get("X-Nafigos-User")
	vaultToken := r.Header.Get("X-Vault-Token")
	admin := (r.Header.Get("X-Nafigos-Admin") == "true")
	r.Header.Del("X-Vault-Token")
	r.Header.Del("X-Nafigos-User")
	r.Header.Del("X-Nafigos-Admin")
	return username, admin, vaultToken
}
