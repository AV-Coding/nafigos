package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/phylax-service/phylax"

	"github.com/gorilla/mux"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
)

// KclusterAPIRouter will route all functions to their API endpoints
func KclusterAPIRouter(router *mux.Router) {
	router.HandleFunc("/kclusters", getAllClusters).Methods("GET")
	router.HandleFunc("/kclusters", addNewCluster).Methods("POST")
	router.HandleFunc("/kclusters/{clusterID}", getCluster).Methods("GET")
	router.HandleFunc("/kclusters/{clusterID}", updateCluster).Methods("PUT")
	router.HandleFunc("/kclusters/{clusterID}", deleteCluster).Methods("DELETE")
	router.HandleFunc("/kclusters/{clusterID}/resources", getClusterResources).Methods("GET")
}

func getAllClusters(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getAllClusters",
	})
	logger.Info("received request to get all kclusters")

	username, _, vaultToken := readAuthHeaders(w, r)
	if len(username) == 0 || len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}
	logger = logger.WithFields(log.Fields{"username": username})
	logger.Info("got username for requesting user")

	clusters, code, err := vaultClient.GetAllClusters(username, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get kclusters:", err)
		jsonError(w, "unable to get kclusters: "+err.Error(), code)
		return
	}

	var logClusters []common.Cluster
	for _, cluster := range clusters {
		logClusters = append(logClusters, cluster.GetRedacted())
	}
	logger = logger.WithFields(log.Fields{"clusters": logClusters})
	logger.Info("successfully got all kclusters")

	clustersBytes, err := json.Marshal(clusters)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal clusters list")
		jsonError(w, "unable to marshal clusters list: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(clustersBytes)
}

func addNewCluster(w http.ResponseWriter, r *http.Request) {
	clusterID := xid.New().String()
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "addNewCluster",
		"cluster_id": clusterID,
	})
	logger.Info("received request to add a new kcluster")

	username, _, vaultToken := readAuthHeaders(w, r)
	if len(username) == 0 || len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}
	logger = logger.WithFields(log.Fields{"username": username})
	logger.Info("got username for requesting user")

	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		jsonError(w, "unable to read request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	var cluster common.Cluster
	err = json.Unmarshal(bodyBytes, &cluster)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse request into kcluster")
		jsonError(w, "unable to parse request into kcluster: "+err.Error(), http.StatusBadRequest)
		return
	}

	// Set Slug if not set
	if len(cluster.Slug) == 0 {
		cluster.Slug = cluster.DefaultNamespace
	}

	logCluster := cluster.GetRedacted()
	logger = logger.WithFields(log.Fields{"cluster": logCluster})
	logger.Info("successfully parsed kcluster")

	code, err := vaultClient.AddCluster(username, clusterID, cluster, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to write kcluster")
		jsonError(w, "unable to write kcluster: "+err.Error(), code)
		return
	}
	cluster.ID = clusterID
	clusterBytes, err := json.Marshal(cluster)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response")
		jsonError(w, "unable to marshal response: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(clusterBytes)
}

func getCluster(w http.ResponseWriter, r *http.Request) {
	clusterID := mux.Vars(r)["clusterID"]
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "getCluster",
		"cluster_id": clusterID,
	})
	logger.Info("received request to get a kcluster")

	username, _, vaultToken := readAuthHeaders(w, r)
	if len(username) == 0 || len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}
	logger = logger.WithFields(log.Fields{"username": username})
	logger.Info("got username for requesting user")

	cluster, code, err := vaultClient.ReadCluster(username, clusterID, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get kcluster")
		jsonError(w, fmt.Sprintf("unable to get kcluster: %v", err), code)
		return
	}

	logCluster := cluster.GetRedacted()
	logger = logger.WithFields(log.Fields{"cluster": logCluster})
	logger.Info("successfully got kcluster")

	clusterBytes, err := json.Marshal(cluster)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response")
		jsonError(w, "unable to marshal response: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(clusterBytes)
}

func updateCluster(w http.ResponseWriter, r *http.Request) {
	clusterID := mux.Vars(r)["clusterID"]
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "updateCluster",
		"cluster_id": clusterID,
	})
	logger.Info("received request to update a kcluster")

	username, _, vaultToken := readAuthHeaders(w, r)
	if len(username) == 0 || len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}
	logger = logger.WithFields(log.Fields{"username": username})
	logger.Info("got username for requesting user")

	// Get new cluster params from request body
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		jsonError(w, "unable to read request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	var cluster common.Cluster
	err = json.Unmarshal(bodyBytes, &cluster)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse request into kcluster")
		jsonError(w, "unable to parse request into kcluster: "+err.Error(), http.StatusBadRequest)
		return
	}

	logCluster := cluster.GetRedacted()
	logger = logger.WithFields(log.Fields{"cluster": logCluster})
	logger.Info("successfully parsed new kcluster")

	code, err := vaultClient.EditCluster(username, clusterID, cluster, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to update kcluster")
		jsonError(w, fmt.Sprintf("unable to update kcluster '%s': %s", clusterID, err.Error()), code)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func deleteCluster(w http.ResponseWriter, r *http.Request) {
	clusterID := mux.Vars(r)["clusterID"]
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "deleteCluster",
		"cluster_id": clusterID,
	})
	logger.Info("received request to delete a kcluster")

	username, _, vaultToken := readAuthHeaders(w, r)
	if len(username) == 0 || len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}
	logger = logger.WithFields(log.Fields{"username": username})
	logger.Info("got username for requesting user")

	code, err := vaultClient.DeleteCluster(username, clusterID, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to delete kcluster")
		jsonError(w, fmt.Sprintf("unable to delete cluster '%s': %s", clusterID, err.Error()), code)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func getClusterResources(w http.ResponseWriter, r *http.Request) {
	clusterID := mux.Vars(r)["clusterID"]
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "getClusterResources",
		"cluster_id": clusterID,
	})
	logger.Info("received request to get kcluster resources")

	username, _, vaultToken := readAuthHeaders(w, r)
	if len(username) == 0 || len(vaultToken) == 0 {
		logger.Error("unable to get info for authenticated user")
		jsonError(w, "unable to get info for authenticated user", http.StatusUnauthorized)
		return
	}
	logger = logger.WithFields(log.Fields{"username": username})
	logger.Info("got username for requesting user")

	namespace := r.FormValue("namespace")
	user := common.User{}
	user.Username = username

	logger = logger.WithFields(log.Fields{"namespace": namespace})

	cluster, code, err := vaultClient.ReadCluster(
		user.Username,
		clusterID,
		vaultToken,
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get kcluster")
		jsonError(w, fmt.Sprintf("unable to get kcluster: %v", err), code)
		return
	}
	user.Clusters = map[string]common.Cluster{}
	user.Clusters[clusterID] = cluster

	logCluster := cluster.GetRedacted()
	logger = logger.WithFields(log.Fields{"cluster": logCluster})
	logger.Info("sending request to Phylax to get cluster resources")

	request := phylax.Request{
		BasicRequest: common.BasicRequest{
			User:      user,
			ClusterID: clusterID,
		},
		Namespace: namespace,
	}
	// Get info from Phylax
	msg, err := common.PublishRequest(
		&request,
		"Phylax.GetResources",
		"API",
		natsInfo["address"],
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish request to Phylax")
		jsonError(w, "unable to get kcluster resources: "+err.Error(), http.StatusInternalServerError)
		return
	}

	// Check for error response to set response header accordingly
	var result struct {
		common.Error `json:"error"`
	}
	err = json.Unmarshal(msg, &result)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read response")
		jsonError(w, "unable to read response: "+err.Error(), http.StatusInternalServerError)
		return
	}
	if result.Code != 0 {
		logger.WithFields(log.Fields{"error": fmt.Errorf(result.Message)}).Error("received error response from Phylax")
		w.WriteHeader(result.Code)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(msg)
}
