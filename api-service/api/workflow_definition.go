package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/workflow-definition-service/workflowdefinition"

	"github.com/gorilla/mux"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
)

// WorkflowDefinitionAPIRouter creates routes necessary for interacting with the
// WorkflowDefinition Service
func WorkflowDefinitionAPIRouter(router *mux.Router) {
	router.HandleFunc("/workflows", getWorkflowDefinitionsForUser).Methods("GET")
	router.HandleFunc("/workflows", createWorkflowDefinition).Methods("POST")
	router.HandleFunc("/workflows/{workflowID}", getWorkflowDefinition).Methods("GET")
	router.HandleFunc("/workflows/{workflowID}", updateWorkflowDefinition).Methods("PUT")
	router.HandleFunc("/workflows/{workflowID}", deleteWorkflowDefinition).Methods("DELETE")
}

func getWorkflowDefinitionsForUser(w http.ResponseWriter, r *http.Request) {
	workflowID := mux.Vars(r)["workflowID"]
	logger := log.WithFields(log.Fields{
		"package":     "api",
		"function":    "getWorkflowDefinitionsForUser",
		"workflow_id": workflowID,
	})
	logger.Info("received request to get WorkflowDefinitions for user")

	var request workflowdefinition.Request
	status, err := prepareRequest(&request, r, workflowID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	msg, err := common.PublishRequest(
		&request,
		"WorkflowDefinition.GetForUser",
		"API",
		natsInfo["address"],
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish request to get WorkflowDefinitions for user")
		jsonError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(getResponseCode(msg))
	w.Write(msg)
}

func createWorkflowDefinition(w http.ResponseWriter, r *http.Request) {
	workflowID := xid.New().String()
	logger := log.WithFields(log.Fields{
		"package":     "api",
		"function":    "createWorkflowDefinition",
		"workflow_id": workflowID,
	})
	logger.Info("received request to create WorkflowDefinition")

	var createRequest workflowdefinition.Request
	status, err := prepareRequest(&createRequest, r, workflowID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	logRequest := createRequest
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	if len(createRequest.URL) == 0 {
		err = fmt.Errorf("'url' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to create WorkflowDefinition without URL")
		jsonError(w, err.Error(), http.StatusBadRequest)
		return
	}
	if len(createRequest.Branch) == 0 {
		createRequest.Branch = "master"
	}
	reqBody, err := json.Marshal(createRequest)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal request JSON")
		jsonError(w, "unable to marshal request JSON: "+err.Error(), http.StatusInternalServerError)
		return
	}
	err = common.StreamingPublish(reqBody, natsInfo, "WorkflowDefinition.Create", "API")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish WorkflowDefinition create request")
		jsonError(w, "unable to publish WorkflowDefinition create request: "+err.Error(), http.StatusInternalServerError)
		return
	}
	idResponse(w, createRequest.GetWorkflowID(), http.StatusAccepted)
}

func getWorkflowDefinition(w http.ResponseWriter, r *http.Request) {
	workflowID := mux.Vars(r)["workflowID"]
	logger := log.WithFields(log.Fields{
		"package":     "api",
		"function":    "getWorkflowDefinition",
		"workflow_id": workflowID,
	})
	logger.Info("received request to get WorkflowDefinition")

	var request workflowdefinition.Request
	status, err := prepareRequest(&request, r, workflowID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	msg, err := common.PublishRequest(
		&request,
		"WorkflowDefinition.Get",
		"API",
		natsInfo["address"],
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish request to get WorkflowDefinition")
		jsonError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(getResponseCode(msg))
	w.Write(msg)
}

// updateWorkflowDefinition will edit a WorkflowDefinition and write these changes
// back to git (asynchronous)
func updateWorkflowDefinition(w http.ResponseWriter, r *http.Request) {
	workflowID := mux.Vars(r)["workflowID"]
	logger := log.WithFields(log.Fields{
		"package":     "api",
		"function":    "updateWorkflowDefinition",
		"workflow_id": workflowID,
	})
	logger.Info("received request to update WorkflowDefinition")

	var updateRequest workflowdefinition.Request
	status, err := prepareRequest(&updateRequest, r, workflowID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	logRequest := updateRequest
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	// Make sure the GitSecretID was included in the request
	if len(updateRequest.GetGitSecretID()) == 0 && r.ContentLength != 0 {
		err := fmt.Errorf("git secret ID is required to update a WorkflowDefinition")
		logger.WithFields(log.Fields{"error": err}).Error("unable to update WorkflowDefinition without git secret")
		jsonError(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Make sure user is authorized for this WFD
	// A new common.Request is created to avoid overwriting important data
	status, err = getWorkflowDefinitionData(&common.BasicRequest{
		WorkflowID: workflowID,
		User:       updateRequest.User,
	})
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get WorkflowDefinition data")
		jsonError(w, "unable to get WorkflowDefinition data: "+err.Error(), status)
		return
	}

	reqBody, err := json.Marshal(&updateRequest)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal request JSON")
		jsonError(w, "unable to marshal request JSON: "+err.Error(), http.StatusInternalServerError)
		return
	}
	err = common.StreamingPublish(reqBody, natsInfo, "WorkflowDefinition.Update", "API")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish update WorkflowDefinition request")
		jsonError(w, "unable to publish update WorkflowDefinition request: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}

func deleteWorkflowDefinition(w http.ResponseWriter, r *http.Request) {
	workflowID := mux.Vars(r)["workflowID"]
	logger := log.WithFields(log.Fields{
		"package":     "api",
		"function":    "deleteWorkflowDefinition",
		"workflow_id": workflowID,
	})
	logger.Info("received request to delete WorkflowDefinition")

	var request workflowdefinition.Request
	status, err := checkAuthorization(&request, r, workflowID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to check authorization")
		jsonError(w, err.Error(), status)
		return
	}
	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	reqBody, err := json.Marshal(&request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal request JSON")
		jsonError(w, "unable to marshal request JSON: "+err.Error(), http.StatusInternalServerError)
		return
	}
	err = common.StreamingPublish(reqBody, natsInfo, "WorkflowDefinition.Delete", "API")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish delete WorkflowDefinition request")
		jsonError(w, "unable to publish delete WorkflowDefinition request: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}
