# Vault

This package provides authentication and secret management for the project. It uses Hashicorp Vault to store user credentials for other services.

More information can be found [here](../../../docs/developers/vault.md)
