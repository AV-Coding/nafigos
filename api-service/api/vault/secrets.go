package vault

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/cyverse/nafigos/common"
)

func secretsPath(owner string) string {
	return namespace + owner + "/secrets/"
}

// AddSecret adds or updates a secret on the user's KV path
func (client *Client) AddSecret(owner string, id string, secret map[string]interface{}, token string) (int, error) {
	return client.write(secretsPath(owner), id, secret, token)
}

// EditSecret will edit/update a secret in Vault
func (client *Client) EditSecret(owner string, id string, newSecret common.Secret, token string) (int, error) {
	// Vault's write will overwrite the entire secret so we need to read it first
	secret, code, err := client.ReadSecret(owner, id, token)
	if err != nil {
		return code, err
	}
	// Assign new attributes
	if len(newSecret.Type) > 0 {
		secret.Type = newSecret.Type
	}
	if len(newSecret.Username) > 0 {
		secret.Username = newSecret.Username
	}
	if len(newSecret.Value) > 0 {
		secret.Value = newSecret.Value
	}
	// Now convert struct to map
	var secretMap map[string]interface{}
	jsonSecret, err := json.Marshal(secret)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	err = json.Unmarshal(jsonSecret, &secretMap)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	return client.write(secretsPath(owner), id, secretMap, token)
}

// DeleteSecret will delete a secret from Vault
func (client *Client) DeleteSecret(owner, id, token string) (int, error) {
	userClient, err := client.getUserClient(token)
	if err != nil {
		return readVaultError(err.Error())
	}
	_, err = userClient.Logical().Delete(secretsPath(owner) + id)
	if err != nil {
		return readVaultError(err.Error())
	}
	return http.StatusOK, nil
}

// ReadSecret will return a secret from Vault
func (client *Client) ReadSecret(owner, id, token string) (common.Secret, int, error) {
	userClient, err := client.getUserClient(token)
	if err != nil {
		code, err := readVaultError(err.Error())
		return common.Secret{}, code, err
	}
	secret, err := userClient.Logical().Read(secretsPath(owner) + id)
	if err != nil {
		code, err := readVaultError(err.Error())
		return common.Secret{}, code, err
	}
	if secret == nil {
		return common.Secret{}, http.StatusBadRequest, fmt.Errorf("secret '%s' not found", id)
	}
	return common.Secret{
		ID:       id,
		Username: secret.Data["username"].(string),
		Value:    secret.Data["value"].(string),
		Type:     common.SecretType(secret.Data["type"].(string)),
	}, http.StatusOK, nil
}

// GetAllSecrets will return all of a user's secrets from Vault
func (client *Client) GetAllSecrets(owner, token string) (secrets map[string]common.Secret, code int, err error) {
	secrets = map[string]common.Secret{}
	allSecrets, err := client.Logical().List(secretsPath(owner))
	if err != nil {
		code, err = readVaultError(err.Error())
		return
	}
	if allSecrets == nil {
		err = fmt.Errorf("No secrets found for user '%s'", owner)
		code = http.StatusNotFound
		return
	}
	for _, id := range allSecrets.Data["keys"].([]interface{}) {
		var secret common.Secret
		secret, code, err = client.ReadSecret(owner, id.(string), token)
		if err != nil {
			return
		}
		secrets[id.(string)] = secret
	}
	return
}
