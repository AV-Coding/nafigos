// Package keycloak provides functions and structs for interacting with a Keycloak
// server. It is a thin wrapper over OpenID Connect and OAuth 2 packages
package keycloak

import (
	"context"

	oidc "github.com/coreos/go-oidc"
	"golang.org/x/oauth2"
)

// Client contains necessary structs for OpenID Connect with Keycloak
type Client struct {
	Provider     *oidc.Provider
	Verifier     *oidc.IDTokenVerifier
	OAuthConfig  oauth2.Config
	OpenIDConfig *oidc.Config
	URL          string
}

// NewClient will create a client for interacting with a Keycloak server
func NewClient(url, redirect, id, secret string) (client *Client, err error) {
	ctx := context.Background()
	client = &Client{URL: url}
	provider, err := oidc.NewProvider(ctx, url)
	if err != nil {
		return
	}
	client.Provider = provider
	client.OAuthConfig = oauth2.Config{
		ClientID:     id,
		ClientSecret: secret,
		RedirectURL:  redirect,
		Endpoint:     provider.Endpoint(),
		Scopes:       []string{oidc.ScopeOpenID, "profile", "email"},
	}
	client.OpenIDConfig = &oidc.Config{ClientID: id}
	client.Verifier = client.Provider.Verifier(client.OpenIDConfig)
	return
}

// Verify contacts the OpenID Connect provider to verify a token
func (client *Client) Verify(token string) (*oidc.IDToken, error) {
	return client.Verifier.Verify(context.Background(), token)
}
