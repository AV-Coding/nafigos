package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
	"time"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/gorilla/mux"
	"github.com/nats-io/nats.go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func testCreateUser(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("EnableUserpassAuth").
		Return(200, nil)
	vaultClient.GetMock().On("MountNewKVPath", "test_username").
		Return(200, nil)
	vaultClient.GetMock().On("CreateNewPolicy", "test_username", false).
		Return(200, nil)
	vaultClient.GetMock().On("WriteUserpass", "test_username", "test_password").
		Return(200, nil)
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("WriteUser", common.User{
		Username: "test_username",
		Password: "test_password",
		Secrets: map[string]common.Secret{
			"1": {
				Type:     "github",
				Username: "test_githubusername",
				Value:    "test_githubtoken",
			},
			"2": {
				Type:     "dockerhub",
				Username: "test_dockerhubusername",
				Value:    "test_dockerhubtoken",
			},
		},
		Clusters: map[string]common.Cluster{
			"1": {
				Name:             "default",
				DefaultNamespace: "default",
				Config:           "kubeconfig",
			},
		},
	}, "vault_token").Return(200, nil)

	req, err := http.NewRequest("POST", "/users", nil)
	req.Header.Add("Authorization", "test_password")
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"username": "test_username",
	"password": "test_password",
	"secrets": {
		"1": {
			"type": "github",
			"username": "test_githubusername",
			"value": "test_githubtoken"
		},
		"2": {
			"type": "dockerhub",
			"username": "test_dockerhubusername",
			"value": "test_dockerhubtoken"
		}
	},
	"clusters": {
		"1": {
			"name": "default",
			"default_namespace": "default",
			"config": "kubeconfig"
		}
	}
}`)))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusCreated, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testCreateUserBadRequest(t *testing.T) {
	req, err := http.NewRequest("POST", "/users", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "unable to parse request body: unexpected end of JSON input"}}`, rr.Body.String())
}

func testCreateUserWithNewKubeconfig(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("EnableUserpassAuth").
		Return(200, nil)
	vaultClient.GetMock().On("MountNewKVPath", "test_username").
		Return(200, nil)
	vaultClient.GetMock().On("CreateNewPolicy", "test_username", false).
		Return(200, nil)
	vaultClient.GetMock().On("WriteUserpass", "test_username", "test_password").
		Return(200, nil)
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("WriteUser", common.User{
		Username: "test_username",
		Password: "test_password",
	}, "vault_token").Return(200, nil)
	vaultClient.GetMock().On("AddCluster",
		"test_username", mock.Anything, mock.Anything, "vault_token",
	).Return(200, nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"Pleo.CreateUserClusterKubeconfig",
		"Pleo.CreateUserClusterKubeconfigQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User common.User `json:"user"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"cluster_config": "NewConfig"}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	req, err := http.NewRequest("POST", "/users", nil)
	req.Header.Add("Authorization", "test_password")
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"username": "test_username",
	"password": "test_password",
	"create_kubeconfig": true
}`)))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)
	handler.ServeHTTP(rr, req)

	// Must sleep briefly to wait for concurrent function to run
	time.Sleep(1 * time.Second)

	assert.Equal(t, http.StatusCreated, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testCreateUserNotEnoughInfoError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}

	req, err := http.NewRequest("POST", "/users", nil)
	req.Header.Add("Authorization", "test_password")
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"username": "",
	"password": "test_password"
}`)))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "Unable to parse DNS 1123 compatible string from: "}}`, rr.Body.String())
}

func testCreateUserPartialInfoNoError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("EnableUserpassAuth").
		Return(200, nil)
	vaultClient.GetMock().On("MountNewKVPath", "test_username2").
		Return(200, nil)
	vaultClient.GetMock().On("CreateNewPolicy", "test_username2", false).
		Return(200, nil)
	vaultClient.GetMock().On("WriteUserpass", "test_username2", "test_password").
		Return(200, nil)
	vaultClient.GetMock().On("GetUserLoginToken", "test_username2", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("WriteUser", common.User{
		Username: "test_username2",
		Password: "test_password",
	}, "vault_token").Return(200, nil)

	req, err := http.NewRequest("POST", "/users", nil)
	req.Header.Add("Authorization", "test_password")
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"username": "test_username2",
	"password": "test_password"
}`)))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusCreated, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testCreateUserErrorCreatingPolicy(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("EnableUserpassAuth").
		Return(200, nil)
	vaultClient.GetMock().On("MountNewKVPath", "test_username").
		Return(200, nil)
	vaultClient.GetMock().On("CreateNewPolicy", "test_username", false).
		Return(500, fmt.Errorf("mock error"))

	req, err := http.NewRequest("POST", "/users", nil)
	req.Header.Add("Authorization", "test_password")
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"username": "test_username",
	"password": "test_password",
	"secrets": {
		"1": {
			"type": "github",
			"username": "test_githubusername",
			"value": "test_githubtoken"
		},
		"2": {
			"type": "dockerhub",
			"username": "test_dockerhubusername",
			"value": "test_dockerhubtoken"
		}
	},
	"clusters": {
		"1": {
			"name": "default",
			"default_namespace": "default",
			"config": "kubeconfig"
		}
	}
}`)))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to create Vault Policy: mock error"}}`, rr.Body.String())
}

func testCreateUserErrorCreatingUserpass(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("EnableUserpassAuth").
		Return(200, nil)
	vaultClient.GetMock().On("MountNewKVPath", "test_username").
		Return(200, nil)
	vaultClient.GetMock().On("CreateNewPolicy", "test_username", false).
		Return(200, nil)
	vaultClient.GetMock().On("WriteUserpass", "test_username", "test_password").
		Return(500, fmt.Errorf("mock error"))

	req, err := http.NewRequest("POST", "/users", nil)
	req.Header.Add("Authorization", "test_password")
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"username": "test_username",
	"password": "test_password",
	"secrets": {
		"1": {
			"type": "github",
			"username": "test_githubusername",
			"value": "test_githubtoken"
		},
		"2": {
			"type": "dockerhub",
			"username": "test_dockerhubusername",
			"value": "test_dockerhubtoken"
		}
	},
	"clusters": {
		"1": {
			"name": "default",
			"default_namespace": "default",
			"config": "kubeconfig"
		}
	}
}`)))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to create Userpass: mock error"}}`, rr.Body.String())
}

func testCreateUserErrorGettingToken(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("EnableUserpassAuth").
		Return(200, nil)
	vaultClient.GetMock().On("MountNewKVPath", "test_username").
		Return(200, nil)
	vaultClient.GetMock().On("CreateNewPolicy", "test_username", false).
		Return(200, nil)
	vaultClient.GetMock().On("WriteUserpass", "test_username", "test_password").
		Return(200, nil)
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("", fmt.Errorf("mock error"))

	req, err := http.NewRequest("POST", "/users", nil)
	req.Header.Add("Authorization", "test_password")
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"username": "test_username",
	"password": "test_password",
	"secrets": {
		"1": {
			"type": "github",
			"username": "test_githubusername",
			"value": "test_githubtoken"
		},
		"2": {
			"type": "dockerhub",
			"username": "test_dockerhubusername",
			"value": "test_dockerhubtoken"
		}
	},
	"clusters": {
		"1": {
			"name": "default",
			"default_namespace": "default",
			"config": "kubeconfig"
		}
	}
}`)))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to login: mock error"}}`, rr.Body.String())
}

func testCreateUserErrorWritingUser(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("EnableUserpassAuth").
		Return(200, nil)
	vaultClient.GetMock().On("MountNewKVPath", "test_username").
		Return(200, nil)
	vaultClient.GetMock().On("CreateNewPolicy", "test_username", false).
		Return(200, nil)
	vaultClient.GetMock().On("WriteUserpass", "test_username", "test_password").
		Return(200, nil)
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("WriteUser", common.User{
		Username: "test_username",
		Password: "test_password",
		Secrets: map[string]common.Secret{
			"1": {
				Type:     "github",
				Username: "test_githubusername",
				Value:    "test_githubtoken",
			},
			"2": {
				Type:     "dockerhub",
				Username: "test_dockerhubusername",
				Value:    "test_dockerhubtoken",
			},
		},
		Clusters: map[string]common.Cluster{
			"1": {
				Name:             "default",
				DefaultNamespace: "default",
				Config:           "kubeconfig",
			},
		},
	}, "vault_token").Return(500, fmt.Errorf("mock error"))

	req, err := http.NewRequest("POST", "/users", nil)
	req.Header.Add("Authorization", "test_password")
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"username": "test_username",
	"password": "test_password",
	"secrets": {
		"1": {
			"type": "github",
			"username": "test_githubusername",
			"value": "test_githubtoken"
		},
		"2": {
			"type": "dockerhub",
			"username": "test_dockerhubusername",
			"value": "test_dockerhubtoken"
		}
	},
	"clusters": {
		"1": {
			"name": "default",
			"default_namespace": "default",
			"config": "kubeconfig"
		}
	}
}`)))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to write user info to Vault: mock error"}}`, rr.Body.String())
}

func testCreateUserDuplicate(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("EnableUserpassAuth").
		Return(200, nil)
	vaultClient.GetMock().On("MountNewKVPath", "test_username").
		Return(400, fmt.Errorf("path is already in use at nafigos/test_username/"))

	req, err := http.NewRequest("POST", "/users", nil)
	req.Header.Add("Authorization", "test_password")
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"username": "test_username",
	"password": "test_password"
}`)))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createUser)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "unable to mount new KV path in Vault: path is already in use at nafigos/test_username/"}}`, rr.Body.String())
}

func testDeleteUser(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("DeleteUser", "test_username").
		Return(200, nil)

	req, err := http.NewRequest("DELETE", "/users/test_username", nil)
	req.Header.Add("Authorization", "test_password")
	assert.NoError(t, err)

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(adminMiddleware)
	router.HandleFunc("/users/{username}", deleteUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusNoContent, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testDeleteUserError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("DeleteUser", "test_username").
		Return(500, fmt.Errorf("mock error"))

	req, err := http.NewRequest("DELETE", "/users/test_username", nil)
	req.Header.Add("Authorization", "test_password")
	assert.NoError(t, err)

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(adminMiddleware)
	router.HandleFunc("/users/{username}", deleteUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to delete user: mock error"}}`, rr.Body.String())
}
