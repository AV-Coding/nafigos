package main

import (
	"fmt"
	"os"
	"sync"
	"time"

	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/phylax-service/phylax"

	log "github.com/sirupsen/logrus"
)

const (
	defaultClusterID   = "nafigos-cluster"
	defaultClientID    = "phylax"
	defaultConnAddress = "nats://localhost:4222"
)

var natsInfo map[string]string

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	err = phylax.InitMongoDB(
		os.Getenv("MONGODB_DB_NAME"),
		os.Getenv("MONGODB_ADDRESS"),
	)
	if err != nil {
		log.WithFields(log.Fields{
			"package":  "main",
			"function": "init",
			"error":    err,
		}).Panic("unable to connect to MongoDB")
	}
	log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	}).Info("successfully connected to MongoDB")

	t := time.Now()
	clientID := fmt.Sprintf("%s-%d", defaultClientID, t.Unix())
	natsInfo = common.GetNATSInfo(defaultClusterID, clientID, defaultConnAddress)
}

func main() {
	monitoringSubjects := map[string]func([]byte, map[string]string){
		"Pleo.Run":    phylax.PleoRun,
		"Pleo.Delete": phylax.PleoDelete,
		"Build.Start": phylax.BuildStart,
		"Build.Job":   phylax.BuildJob,
	}
	var wg sync.WaitGroup
	wg.Add(4 + len(monitoringSubjects))
	for subject, handler := range monitoringSubjects {
		go common.StreamingSubscriber(subject, handler, natsInfo, &wg)
	}
	go common.SynchronousSubscriber("Phylax.GetRunner", "Phylax.GetRunnerQueue", phylax.GetRunner, natsInfo, &wg)
	go common.SynchronousSubscriber("Phylax.GetRunnersForUser", "Phylax.GetRunersForUser", phylax.GetRunnersForUser, natsInfo, &wg)
	go common.SynchronousSubscriber("Phylax.GetBuild", "Phylax.GetBuildQueue", phylax.GetBuild, natsInfo, &wg)
	go common.SynchronousSubscriber("Phylax.GetBuildsForUser", "Phylax.GetRunersForUser", phylax.GetBuildsForUser, natsInfo, &wg)
	go common.SynchronousSubscriber("Phylax.GetResources", "Phylax.GetResourcesQueue", phylax.GetResources, natsInfo, &wg)
	go common.StreamingQueueSubscriber("Phylax.UpdateRunner", "Phylax.UpdateRunnerQueue", phylax.UpdateRunner, natsInfo, &wg)
	wg.Wait()
}
