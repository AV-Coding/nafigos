package common

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetRedactedSecret(t *testing.T) {
	secret := &Secret{
		Username: "Username",
		Value:    "Value",
		Type:     GitSecret,
		ID:       "ID",
	}
	redactedSecret := secret.GetRedacted()
	assert.Equal(t, "REDACTED", redactedSecret.Value)
	// make sure original Secret was not modified
	assert.Equal(t, "Value", secret.Value)
}

func TestGetRedactedCluster(t *testing.T) {
	cluster := &Cluster{
		ID:               "ID",
		Name:             "Name",
		DefaultNamespace: "DefaultNamespace",
		Config:           "Config",
		Host:             "Host",
		Slug:             "Slug",
	}
	redactedCluster := cluster.GetRedacted()
	assert.Equal(t, "REDACTED", redactedCluster.Config)
	// make sure original Cluster was not modified
	assert.Equal(t, "Config", cluster.Config)
}

func TestGetRedactedUser(t *testing.T) {
	user := &User{
		Username: "test-user",
		Password: "so secret",
		Clusters: map[string]Cluster{
			"ID": {ID: "ID", Config: "NOT REDACTED"},
		},
		Secrets: map[string]Secret{
			"ID": {ID: "ID", Value: "NOT REDACTED"},
		},
	}
	redactedUser := user.GetRedacted()
	assert.Equal(t, "REDACTED", redactedUser.Secrets["ID"].Value)
	assert.Equal(t, "REDACTED", redactedUser.Clusters["ID"].Config)
	assert.Equal(t, "REDACTED", redactedUser.Password)
	// make sure original User was not modified
	assert.Equal(t, "NOT REDACTED", user.Secrets["ID"].Value)
	assert.Equal(t, "NOT REDACTED", user.Clusters["ID"].Config)
	assert.Equal(t, "so secret", user.Password)
}
