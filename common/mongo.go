package common

import (
	"context"
	"reflect"
	"time"

	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// CRUD is an interface that allows for mocking of the MongoDB
type CRUD interface {
	GetMock() *mock.Mock
	Create(string, NafigosBase) error
	Read(string, string) ([]byte, error)
	Update(string, NafigosBase) error
	Replace(string, NafigosBase) error
	Delete(string, string) error
	ReadForUser(string, string) ([]map[string]interface{}, error)
}

// DatabaseAdapter implements the CRUD interface to allow for mocking
type DatabaseAdapter struct {
	*mongo.Database
}

// GetMongoDBAdapter returns a new connection to MongoDB and wraps it in a CRUD interface
func GetMongoDBAdapter(name, address string) (adapter DatabaseAdapter, err error) {
	client, err := mongo.Connect(
		context.TODO(),
		options.Client().ApplyURI(address),
	)
	if err != nil {
		return
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return
	}
	adapter = DatabaseAdapter{
		client.Database(name),
	}

	return
}

// GetMock ...
func (d DatabaseAdapter) GetMock() (m *mock.Mock) {
	return
}

// Create will insert into the database
func (d DatabaseAdapter) Create(collectionName string, document NafigosBase) (err error) {
	_, err = d.Collection(collectionName).InsertOne(context.TODO(), document)

	return
}

// Read will read from the database
func (d DatabaseAdapter) Read(collectionName string, id string) (data []byte, err error) {
	ctx, _ := context.WithTimeout(context.TODO(), 10*time.Second)
	result := d.Collection(collectionName).FindOne(ctx, bson.M{"_id": id})
	data, err = result.DecodeBytes()
	return
}

// Update wil update/edit in the database
func (d DatabaseAdapter) Update(collectionName string, document NafigosBase) (err error) {
	t := reflect.TypeOf(document)
	var updateFields bson.M

	for i := 0; i < t.NumField(); i++ {
		fieldName := t.Field(i).Tag.Get("json")
		fieldValue := reflect.ValueOf(document).Elem().Field(i)
		updateFields[fieldName] = fieldValue
	}

	update := bson.M{
		"$set": updateFields,
	}

	filter := bson.D{{"_id", document.GetID()}}
	_, err = d.Collection(collectionName).UpdateOne(context.TODO(), filter, update)

	return
}

// Replace will save/replace an existing object in the database
func (d DatabaseAdapter) Replace(collectionName string, document NafigosBase) (err error) {
	filter := bson.D{{"_id", document.GetID()}}
	upsert := (&options.ReplaceOptions{}).SetUpsert(true)
	_, err = d.Collection(collectionName).ReplaceOne(context.TODO(), filter, document, upsert)
	return
}

// Delete will delete from the database
func (d DatabaseAdapter) Delete(collectionName, id string) (err error) {
	_, err = d.Collection(collectionName).DeleteOne(context.TODO(), bson.M{"_id": id})
	return
}

// ReadForUser will return all objects owned by a user
func (d DatabaseAdapter) ReadForUser(collectionName, owner string) (results []map[string]interface{}, err error) {
	cursor, _ := d.Collection(collectionName).Find(context.TODO(), bson.M{"owner": owner})
	defer cursor.Close(context.TODO())
	err = cursor.All(context.TODO(), &results)
	return
}

// MockedAdapter for testing
type MockedAdapter struct {
	Mock *mock.Mock
}

// GetMock ...
func (d MockedAdapter) GetMock() *mock.Mock {
	return d.Mock
}

// Create ...
func (d MockedAdapter) Create(collectionName string, document NafigosBase) error {
	args := d.GetMock().Called(document)
	return args.Error(0)
}

// Read ...
func (d MockedAdapter) Read(collectionName, id string) ([]byte, error) {
	args := d.GetMock().Called(id)
	return args.Get(0).([]byte), args.Error(1)
}

// Update ...
func (d MockedAdapter) Update(collectionName string, document NafigosBase) error {
	args := d.GetMock().Called(document)
	return args.Error(0)
}

// Replace ...
func (d MockedAdapter) Replace(collectionName string, document NafigosBase) error {
	args := d.GetMock().Called(document)
	return args.Error(0)
}

// Delete ...
func (d MockedAdapter) Delete(collectionName, id string) error {
	args := d.GetMock().Called(id)
	return args.Error(0)
}

// ReadForUser ...
func (d MockedAdapter) ReadForUser(collectionName, owner string) ([]map[string]interface{}, error) {
	args := d.GetMock().Called(owner)
	return args.Get(0).([]map[string]interface{}), args.Error(1)
}
