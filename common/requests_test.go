package common

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var request = BasicRequest{
	User: User{
		Username: "test-user",
		Clusters: map[string]Cluster{
			"ID": {ID: "ID", Config: "NOT REDACTED"},
		},
		Secrets: map[string]Secret{
			"ID": {ID: "ID", Value: "NOT REDACTED"},
		},
	},
	ClusterID:        "clusterID",
	RegistrySecretID: "registrySecretID",
	GitSecretID:      "gitSecretID",
	WorkflowID:       "workflowID",
	BuildID:          "buildID",
	RunID:            "runID",
	Error: Error{
		Message: "message",
		Code:    500,
	},
}

func TestGetRedactedRequest(t *testing.T) {
	redactedRequest := request.GetRedacted()
	assert.Equal(t, "REDACTED", redactedRequest.User.Secrets["ID"].Value)
	assert.Equal(t, "REDACTED", redactedRequest.User.Clusters["ID"].Config)
	// make sure original request was not modified
	assert.Equal(t, "NOT REDACTED", request.User.Secrets["ID"].Value)
	assert.Equal(t, "NOT REDACTED", request.User.Clusters["ID"].Config)
}

func TestGetClusterID(t *testing.T) {
	assert.Equal(t, "clusterID", request.GetClusterID())
}

func TestGetRegistrySecretID(t *testing.T) {
	assert.Equal(t, "registrySecretID", request.GetRegistrySecretID())
}

func TestGetGitSecretID(t *testing.T) {
	assert.Equal(t, "gitSecretID", request.GetGitSecretID())
}

func TestGetWorkflowID(t *testing.T) {
	assert.Equal(t, "workflowID", request.GetWorkflowID())
}

func TestGetBuildID(t *testing.T) {
	assert.Equal(t, "buildID", request.GetBuildID())
}

func TestGetRunID(t *testing.T) {
	assert.Equal(t, "runID", request.GetRunID())
}

func TestGetUser(t *testing.T) {
	assert.Equal(t, "test-user", request.GetUser().Username)
}

func TestGetError(t *testing.T) {
	err := request.GetError()
	assert.Equal(t, "message", err.Message)
	assert.Equal(t, 500, err.Code)
}

func TestSetClusterID(t *testing.T) {
	request.SetClusterID("newClusterID")
	assert.Equal(t, "newClusterID", request.GetClusterID())
}

func TestSetRegistrySecretID(t *testing.T) {
	request.SetRegistrySecretID("newRegistrySecretID")
	assert.Equal(t, "newRegistrySecretID", request.GetRegistrySecretID())
}

func TestSetGitSecretID(t *testing.T) {
	request.SetGitSecretID("newGitSecretID")
	assert.Equal(t, "newGitSecretID", request.GetGitSecretID())
}

func TestSetWorkflowID(t *testing.T) {
	request.SetWorkflowID("newWorkflowID")
	assert.Equal(t, "newWorkflowID", request.GetWorkflowID())
}

func TestSetBuildID(t *testing.T) {
	request.SetBuildID("newBuildID")
	assert.Equal(t, "newBuildID", request.GetBuildID())
}

func TestSetRunID(t *testing.T) {
	request.SetRunID("newRunID")
	assert.Equal(t, "newRunID", request.GetRunID())
}

func TestSetUser(t *testing.T) {
	request.SetUser(User{Username: "new-user"})
	assert.Equal(t, "new-user", request.GetUser().Username)
}
