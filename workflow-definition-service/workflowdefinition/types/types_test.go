package types

import (
	"testing"

	"gitlab.com/cyverse/nafigos/common"

	argo "github.com/argoproj/argo/pkg/apis/workflow/v1alpha1"
	"github.com/stretchr/testify/assert"
	core "k8s.io/api/core/v1"
)

func TestCreateDeployment(t *testing.T) {
	deployment := createDeployment([]core.Container{{}}, "XID", "NAME", "BRANCH")
	assert.Equal(t, "NAME", deployment.ObjectMeta.Name)
	assert.Equal(t, "NAME", deployment.ObjectMeta.Labels["app"])
	assert.Equal(t, "BRANCH", deployment.ObjectMeta.Labels["version"])
	assert.Equal(t, "XID", deployment.ObjectMeta.Labels["nafigos_id"])
	assert.Len(t, deployment.Spec.Template.Spec.Containers, 1)
}

func TestContainerWorkflowType(t *testing.T) {
	cw := ContainerWorkflow([]core.Container{})
	assert.Equal(t, "ContainerWorkflow", cw.Type())
}

func TestArgoWorkflowType(t *testing.T) {
	aw := ArgoWorkflow{&argo.Workflow{}}
	assert.Equal(t, "ArgoWorkflow", aw.Type())
}

func TestNewArgoWorkflow(t *testing.T) {
	nw, err := NewArgoWorkflow([]byte(`
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: nafigos-say-
spec:
  entrypoint: steps
  templates:
  - name: steps
    steps:
     - - name: A
         template: nafigos-say
       - name: B
         template: nafigos-say-soft
  - name: nafigos-say
    container:
      image: image-1
      command: [cowsay]
  - name: nafigos-say-soft
    container:
      image: image-2
      command: [cowsay]
`))
	assert.NoError(t, err)
	assert.Equal(t, "nafigos-say-", nw.(ArgoWorkflow).Workflow.GenerateName)
	assert.Equal(t, "ArgoWorkflow", nw.Type())
}

func TestNewContainerWorkflow(t *testing.T) {
	nw, err := NewContainerWorkflow([]byte(`
- image: image-1
  name: nginx-container
  ports:
  - containerPort: 80
    name: frontend
- image: training/webapp
  name: webapp-container
  ports:
  - containerPort: 5000
    name: backend
`))
	assert.NoError(t, err)
	assert.Len(t, nw.(ContainerWorkflow), 2)
	assert.Equal(t, "ContainerWorkflow", nw.Type())
}

func TestMockWorkflow(t *testing.T) {
	mw, err := NewMockWorkflow([]byte("This is a workflow"))
	assert.NoError(t, err)
	assert.Equal(t, "MockWorkflow", mw.Type())
	err = mw.Run(&common.K8sClientsets{}, map[string]interface{}{}, "name", "namespace")
	assert.NoError(t, err)
	err = mw.SetupRouting(&common.K8sClientsets{}, &common.K8sClientsets{}, map[string]interface{}{}, "name", "namespace", common.Cluster{})
	assert.NoError(t, err)
	err = mw.Delete(&common.K8sClientsets{}, "name", "namespace")
	assert.NoError(t, err)
	err = mw.ScaleFromZero(&common.K8sClientsets{}, map[string]interface{}{}, "name", "namespace")
	assert.NoError(t, err)
}

func TestParseRawWorkflow(t *testing.T) {
	w, err := ParseRawWorkflow(`
- image: {{image 1}}
  name: nginx-container
  ports:
  - containerPort: 80
    name: frontend
- image: training/webapp
  name: webapp-container
  ports:
  - containerPort: 5000
    name: backend
`, "container", []*common.BuildStep{{Image: "image-1"}})
	assert.NoError(t, err)
	assert.Equal(t, "ContainerWorkflow", w.Type())
	assert.Len(t, w.(ContainerWorkflow), 2)
}

func TestParseRawWorkflowTemplateError(t *testing.T) {
	_, err := ParseRawWorkflow(`
- image: {{image 1}}
  name: nginx-container
  ports:
  - containerPort: 80
    name: frontend
- image: training/webapp
  name: webapp-container
  ports:
  - containerPort: 5000
    name: backend
`, "container", []*common.BuildStep{})
	assert.Error(t, err)
	assert.Equal(t, "Error parsing Workflow template:", err.Error()[0:len("Error parsing Workflow template:")])
}

func TestParseRawWorkflowErrorNonexistentType(t *testing.T) {
	_, err := ParseRawWorkflow(`non-existent workflow`, `non-existent`, []*common.BuildStep{})
	assert.Error(t, err)
	assert.Equal(t, "Error creating 'non-existent' Workflow from string: no constructor for Workflow Type 'non-existent'", err.Error())
}

func TestParseRawWorkflowConstructorError(t *testing.T) {
	_, err := ParseRawWorkflow("rawWorkflow", "container", []*common.BuildStep{})
	assert.Error(t, err)
	assert.Equal(t, "Error creating 'container' Workflow from string: error unmarshaling JSON: while decoding JSON: json: cannot unmarshal string into Go value of type types.ContainerWorkflow", err.Error())
}
