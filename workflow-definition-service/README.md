# Workflow Definition Service

The WorkflowDefintion Service is responsible for reading a YAML file from a git repository in able to gather information for Build and Run. It acts as a mediator between the user and these other services since they rely on information from this service.

[Read more...](../docs/developers/workflow-definition-service.md)
