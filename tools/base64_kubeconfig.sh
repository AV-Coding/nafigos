#!/bin/bash
DOCKER_IP=$(ip addr show docker0 | grep -Po 'inet \K[\d.]+')
echo $(\
	kind get kubeconfig --name user-cluster |\
	sed "s/server:.*$/server: https:\/\/${DOCKER_IP}:36443/" |\
	sed "s/certificate-authority-data:.*$/insecure-skip-tls-verify: true/" |\
	base64 -w0\
)
