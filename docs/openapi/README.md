This directory contains the OpenAPI docs for nafigos. The primary file will be `nafigos-openapi.yaml`.

Editors that work with OpenAPI v3+ include:
* vscode with OpenAPI (Swagger) Editor plugin
* https://editor.swagger.io/

If you want to use a UI to view the OpenAPIs locally, you can use swagger ui in a docker container. For example, let's say you want to install the open api in /opt and assuming the current user has docker access:
* export BASE_DIR=/opt
* `cd $BASE_DIR` (optional)
* `git clone https://gitlab.com/cyverse/nafigos`
* `docker pull swaggerapi/swagger-ui`
* `docker run -p8080:8080 --rm -e URL=http://localhost:8080/openapi/nafigos-openapi.yaml -v ${BASE_DIR}/nafigos/docs/openapi/:/usr/share/nginx/html/openapi/ swaggerapi/swagger-ui`
* open browser to http://localhost:8080/
