# OpenFaaS Workflow

<span style="color:red">**OpenFaaS Workflows are not currently supported everywhere. Functions are only available in the namespace specified during the OpenFaaS Operator install. [MORE INFO HERE.](https://cyverse.atlassian.net/browse/NAFIGOS-190)**</span>

This document contains information about the OpenFaaS Workflow type. This relies on OpenFaaS Functions to run Workflows in Nafigos and a single Nafigos Workflow can include multiple OpenFaaS functions. The best place to learn more about OpenFaaS Functions is in the official [OpenFaaS Documentation](https://docs.openfaas.com/).

OpenFaaS exposes HTTP of functions on port 8080, so make sure to include that in your `http_ports` field:
```yaml
http_ports:
- name: openfaas
  port: 8080
  targetPort: 8080
```


## Table of Contents
[[_TOC_]]


## Example

[See full example here](https://gitlab.com/cyverse/nafigos-openfaas/)

```yaml
name: nafigos-openfaas
build:
- dockerfile: Dockerfile
  image: nafigostest/hello-python:latest
description: Basic OpenFaaS Hello World example
http_ports:
- name: openfaas
  port: 8080
  targetPort: 8080
type: openfaas
workflow: |
  provider:
    name: openfaas
  functions:
    hello-python:
      lang: python
      handler: ./hello-python
      image: {{image 1}}
      environment:
        GREETING: hello
```


## Creating OpenFaaS Function WorkflowDefinitions

Running OpenFaaS Functions in Kubernetes is pretty interesting because it is not generally done with a Kubernetes Custom Resource Definition, and instead the OpenFaaS CLI will generate the CRD YAML from the Function YAML before deploying to Kubernetes. To simplify things for users, Nafigos will take that OpenFaaS YAML and handle the conversion. In order to make your git repository compatible with Nafigos image building, this will require some additional steps to make sure all the necessary resources are present.

In order for your git repository to be compatible with building the container image, you will have to use the `faas-cli build` command. You will use the `--shrinkwrap` command to generate the build resources rather than building the image outright:

```shell
faas-cli build --shrinkwrap -f ./hello-python.yml
```

This will create a `build` directory containing everything you need. The contents of the `build` directory's subdirectory need to be at the root of your WFD repository. Using the [OpenFaaS "First Python Function"](https://docs.openfaas.com/tutorials/first-python-function) as an example:

1. Create directory for this work
    ```shell
    mkdir nafigos-openfaas
    ```

2. Now use the `faas-cli` to generate the scaffolding
    ```shell
    faas-cli new --lang python hello-python
    ```

3. Edit the `hello-python/handler.py` file to have a simple Hello World function:
    ```python
    def handle(req):
        print("Hello! You said: " + req)
    ```

4. Take a look at the `hello-python.yml` file; this will be the `workflow` portion of your `wfd.yaml`

5. The next step in the OpenFaaS docs is to run `faas-cli build -f ./hello-python.yml`, but this is where we will differ. Instead, run this:
    ```shell
    faas-cli build --shrinkwrap -f ./hello-python.yml
    ```
    - This will create a `build/hello-python` directory. The contents of this directory are all you need 

6. Now `cd` into the generated directory, create your `wfd.yaml` file and initialize your git repository
