# Workflow Definition
This document contains information for creating a `wfd.yaml` file for your repository.

## Table of Contents
[[_TOC_]]


## Workflow Types

| Name                                          | Type Key               | Description                                |
| :-------------------------------------------- | :--------------------- | :----------------------------------------- |
| [Container Workflow](./container_workflow.md) | `container`, `default` | This is the default, simplest Workflow Type using the `containers` field of a [Kubernetes Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) |
| [Argo Workflow](./argo_workflow.md)           | `argo`                 | This will run an [Argo Workflow](https://github.com/argoproj/argo/blob/master/examples/README.md) |
| [OpenFaaS Workflow](./openfaas_workflow.md)   | `openfaas`             | This will run [OpenFaaS Functions](https://docs.openfaas.com/) <span style="color:red">**OpenFaaS Workflows are not currently supported everywhere. Functions are only available in the namespace specified during the OpenFaaS Operator install. [MORE INFO HERE.](https://cyverse.atlassian.net/browse/NAFIGOS-190)**</span> |


## Examples

A few examples of WorkflowDefinition repos to try:
  - https://gitlab.com/cyverse/nafigos-helloworld
  - https://gitlab.com/cyverse/nafigos-simple-http-counter
  - https://gitlab.com/cyverse/nafigos-postgres
  - https://gitlab.com/cyverse/nafigos-helloworld-argo
  - https://gitlab.com/cyverse/nafigos-openfaas/


## Fields

### name
```yaml
name: nafigos-helloworld
```
This is simply the name of the Workflow. It must follow [Kubernetes naming rules](https://kubernetes.io/docs/concepts/overview/working-with-objects/names/), which basically means it can only have lowercase alphanumeric characters, `-` or `.` and cannot be more than 253 characters. 


### description
```yaml
description: This runs a simple hello world Go webapp behind an Nginx proxy
```
This description can be anything you want and is just used to provide additional information about your Workflow.


### type
```yaml
type: container
```
The type is required to tell Nafigos how to parse your Workflow. See the ["Workflow Types"](#workflow-types) section above for more information.


### http_ports
```yaml
http_ports:
- name: frontend
  port: 81
  targetPort: frontend
```
This field is a list of [Kubernetes ServicePorts](https://kubernetes.io/docs/concepts/services-networking/service/#multi-port-services) to provide mapping to ports used by containers in the workflow. The `port` field will be appended to the URL for accessing your HTTP Workflow.


### tcp_ports
```yaml
tcp_ports:
- name: postgres
  port: 5432
  targetPort: 5432
```
This is also a list of [Kubernetes ServicePorts](https://kubernetes.io/docs/concepts/services-networking/service/#multi-port-services) to provide mapping to container ports. However, instead of appending the the URL, these ports are exposed using randomly-assigned `NodePorts` which can be gotten from the Runner information and used to access your containers. This is useful for databases and other TCP services. See [this example](https://gitlab.com/cyverse/nafigos-postgres).


### build
```yaml
build:
- image: nafigostest/nafigos-say:{{commit}}
  dockerfile: Dockerfile
- image: nafigostest/nafigos-say:{{branch}}
  dockerfile: Dockerfile
  args:
    - name: filename
      value: soft.cow
```
This is an important field used for building container images from your repository. Multiple builds can be specified here and the `image` field allowsd the use of two variables:
  - `commit`: the full commit hash of the current commit
  - `branch`: the name of the branch used for this Workflow

Each build in the list can define it's own Dockerfile to use as well as a list of build args for the container.


### workflow
```yaml
workflow: |
  - image: {{image 1}}
    name: nginx-container
    ports:
    - containerPort: 80
      name: frontend
  - image: training/webapp
    name: webapp-container
    env:
    - name: PROVIDER
      value: nafigos
```
This field is a string containing the actual content of the Workflow. It is important that it is used as a string rather than more YAML since it needs to be parsed differently depending on the Workflow Type. This field also allows the use of variables to use image names from the `build` field by number, starting at 1.
