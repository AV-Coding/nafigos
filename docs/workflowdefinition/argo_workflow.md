# Argo Workflow
This document contains information about the Argo Workflow type. Since this is just a raw Argo Workflow, the best place to find documentation is the [official Argo docs](https://github.com/argoproj/argo/blob/master/examples/README.md).

**Note**: Due to the current solution for HTTP routing to Argo Workflows (which generally may not directly run on HTTP ports), including port 80 in the `http_ports` field is required:
```yaml
http_ports:
- port: 80
  targetPort: 80
```


## Table of Contents
[[_TOC_]]


## Example

[See full example here](https://gitlab.com/cyverse/nafigos-helloworld-argo)

```yaml
name: nafigos-say
description: This hello world example uses a modified whalesay container to output fun text
type: argo
build:
- image: nafigostest/nafigos-say:latest
  dockerfile: Dockerfile
- image: nafigostest/nafigos-say:soft
  dockerfile: Dockerfile
  args:
    - name: filename
      value: soft.cow
http_ports:
- port: 80
  targetPort: 80
workflow: |
  apiVersion: argoproj.io/v1alpha1
  kind: Workflow
  metadata:
    generateName: nafigos-say-
  spec:
    entrypoint: steps
    templates:
    - name: steps
      steps:
       - - name: A
           template: nafigos-say
         - name: B
           template: nafigos-say-soft
    - name: nafigos-say
      container:
        image: {{image 1}}
        command: [cowsay]
    - name: nafigos-say-soft
      container:
        image: {{image 2}}
        command: [cowsay]
```
