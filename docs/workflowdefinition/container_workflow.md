# Container Workflow
This document contains information about the default Container Workflow type. Since this is just the `containers` field of a Kubernetes Deployment, the best way to find more information is in the [official Kubernetes docs](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/).


## Table of Contents
[[_TOC_]]


## Example

[See full example here](https://gitlab.com/cyverse/nafigos-helloworld/) and [here](https://gitlab.com/cyverse/nafigos-postgres)

```yaml
name: nafigos-helloworld
description: This runs a simple hello world Go webapp behind an Nginx proxy
type: container
http_ports:
- name: frontend
  port: 81
  targetPort: frontend
build:
- image: nafigostest/nafigos-webapp-nginx
  dockerfile: "Dockerfile"
workflow: |
  - image: {{image 1}}
    name: nginx-container
    ports:
    - containerPort: 80
      name: frontend
  - image: training/webapp
    name: webapp-container
    env:
    - name: PROVIDER
      value: nafigos
```
