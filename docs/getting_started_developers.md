# Getting Started

This document will outline how to perform a basic test of all the services to make sure they are all working as expected. This assumes you already have the Nafigos CLI built and your development clusters up and running by following the [instructions here](../install/README.md).

Mode developer documentation can be found [here](./developers/README.md).

### Table of Contents
[[_TOC_]]


### Development Tools

For local development, we prefer to use [Skaffold](https://skaffold.dev/) which provides automatic rebuild and redeploy of containers in Kubernetes when it detects code changes.

We use [`dep`](https://github.com/golang/dep) for dependency management.

In order to pass the GitLab CI Pipeline for a Merge Request, your branch will need to pass various checks including dependency check, format check, and linting:
  - `dep check` is used to make sure dependencies are in a valid state
  - `gofmt -s` is used to format code. The `-s` flag is used to simplify. It is recommended to make this an automatic step handled by your IDE/editor whenever you save a file
  - `golint` is used for linting


### First Steps

If you chose to use Skaffold for local development, start it up from the root of the `nafigos` directory:
```bash
skaffold dev
```

Now, make sure you have these environment variables defined:
```bash
# with service-cluster context:
export SERVICE_GATEWAY_URL=$(kubectl --context=kind-service-cluster get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl --context=kind-service-cluster -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export NAFIGOS_API=$SERVICE_GATEWAY_URL

# with user-cluster context:
export USER_GATEWAY_URL=$(kubectl --context=kind-user-cluster get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl --context=kind-user-cluster -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
```

or if you are using HA Proxy locally, you can just do:
```bash
export NAFIGOS_API=localhost
export USER_GATEWAY_URL=localhost:8080
```

If you are using Keycloak, define the following extra variable in the service cluster context:
```bash
export KEYCLOAK_URL=$(kubectl --context=kind-service-cluster get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl --context=kind-service-cluster get service keycloak -o jsonpath='{.spec.ports[?(@.name=="keycloak")].nodePort}')
```
Then, visit that address in your browser, login with user `admin` and password `admin`. Create a new user. Make sure to add the `nafigos_admin` role in `Role Mappings`.

### Logging in and creating a user

If using Keycloak, visit `$NAFIGOS_API/user/login` to login as the user created in Keycloak and copy the `IDToken` to export in the `NAFIGOS_TOKEN` environment variable.

Otherwise, set `NAFIGOS_TOKEN` to some password (the rest of this will assume your password is `password`)

Now, you are ready to create the user in Nafigos. First, create a JSON file similar to the following:
```json
{
	"username": "username",
	"password": "password (not necessary with Keycloak)",
	"is_admin": true,
	"secrets": {
		"1": {
			"type": "git",
			"username": "github_username",
			"value": "github_token"
		},
		"2": {
			"type": "dockerhub",
			"username": "dockerhub_username",
			"value": "dockerhub_token"
		}
	},
	"clusters": {
		"1": {
			"name": "default",
			"default_namespace": "username",
			"config": "BASE64_KUBECONFIG",
			"host": "*"
		}
	}
}
```
  - You do not need to set the `secrets` and `clusters`; they can be added later
  - The `BASE64_KUBECONFIG` for the Kind user-cluster can be gotten by running the [`base64_kubeconfig.sh`](../tools/kind/base64_kubeconfig.sh) script

Now, create the user in Nafigos:
```bash
nafigos create user -f user-request.json
# or, skip the JSON file and use
nafigos create user -admin -username ${username} -password ${password}
# Once again, skip the password if using Keycloak
```

### Next Steps
Now you have everything you need to interact with your user's resources in Nafigos, specifically the K8s cluster info and secrets. Check out the [Getting Started (users) doc](./getting_started_users.md) to learn more about interacting with Nafigos using the CLI.


### TL;DR
```bash
# From the root of nafigos directory
./tools/kind/create_kind_clusters.sh

export SERVICE_GATEWAY_URL=$(kubectl --context=kind-service-cluster get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl --context=kind-service-cluster -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export NAFIGOS_API=$SERVICE_GATEWAY_URL
export USER_GATEWAY_URL=$(kubectl --context=kind-user-cluster get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl --context=kind-user-cluster -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')

nafigos create user -f user-request.json
export clusterID=$(nafigos get kcluster | jq -r 'keys'[0])
export wfdID=$(nafigos create wfd -url ${git_url} -branch ${git_branch} | jq -r .id)
# Then, get secretID. If you have one secret, this could be:
export secretID=$(nafigos get secret ${username} | jq -r 'keys'[0])
export buildID =$(nafigos build -registry-secret ${secretID} -cluster ${clusterID} ${wfdID} | jq -r .id)
export runID=$(nafigos run -cluster ${clusterID} ${wfdID} | jq -r .id)
```
