# Phylax

Monitoring for Nafigos is provided by the Phylax service.  Here's how to add
a NATS subject to Phylax monitoring:

1.  Edit `phylax/phylax-service/main.go`.  Add an entry to the `monitoringSubjects`
map, with the key being a string representing the NATS subject you want to monitor
and the value being a handler function.  By convention, the subject should be
something like `<Service>.<Action>` and the handler function should be `phylax.<Serice><Action>`

2. Implement your handler function somewhere in Phylax.  If there's no `.go` file for the
type/service that you're monitoring (e.g. `workflowdefinition.go`), make a new one.  Your
handler function must conform to the type specification for `stan.MsgHandler`, which means
it must accept `*stan.Msg` as an argument and not return a value
