# API Service

This service is used as an intermediary between the user (or web UI) and the rest of the microservices.

This service will interact with the other services with a mix of synchronous and asynchronous requests. Queries (often `GET` requests) will synchronously return information about an object. Commands will asynchronously trigger events in other services such as creating a WorkflowDefinition or starting a build.

### Table of Contents
[[_TOC_]]


## Environment

| Variable name         | Required | Default                                    | Description                                |
| :-------------------- | :------- | :----------------------------------------- | :----------------------------------------- |
| NATS_CLUSTER_ID       | yes      | `nafigos-cluster`                          | ID of the cluster to connect to            |
| NATS_CLIENT_ID        | yes      | `api`                                      | ID of _this_ client                        |
| NATS_ADDRESS          | yes      | `nats://localhost:4222`                    | address of the NATS Streaming server       |
| AUTH_METHOD           | no       | `""`                                       | Authentication method. Use "keycloak" when using Keycloak. Defaults to userpass auth |
| KEYCLOAK_URL          | no       | `http://keycloak:8080/auth/realms/nafigos` | URL to access Keycloak Realm               |
| KEYCLOAK_REDIRECT_URL | no       | `http://api:8080/user/login/callback`      | Nafigos API url to redirect to after login |
| KEYCLOAK_SECRET       | no       |                                            | Secret value provided by Keycloak          |


## OpenAPI Specification
This API's specification can be found [here](../openapi/nafigos-openapi.yaml).


## Usage
The best way to interact with the Nafigos API is with the [`nafigos` command](../../cmd/README.md)

Alternatively, you can use curl commands like this:
```bash
curl -X GET -H "Authorization: ${NAFIGOS_TOKEN}" $NAFIGOS_API/workflows/${workflowID}
```


## Developers
The API Service is developed using the [Gorilla `mux`](https://github.com/gorilla/mux) package. This package implements a request router for matching HTTP requests and dispatching them to the correct handler function. It has great compatibility with the builtin `net/http` package and uses a lot of structs and functions defined there, making it really intuitive and compatible with a lot of other things.


### Routing
The API Service code is organized with files for each top-level path in the route which often corresponds to the different services that it interacts with. These files will each export a function named something like `WorkflowDefinitionAPIRouter(router *mux.Router)` to handle routing the specific paths and handlers in that file. The `main` function of the API Service will call each of these functions to initialize the routes.

It is important to know that the order in which the routes are initialized is important. **The paths are matched in the order that they are defined.**


### Authentication Middleware
When calling the functions to initialize routes, the `main` method will pass a router to these functions that was created with the `AuthenticationMiddleware` setup.

A "middleware" is a function or handler that acts as an intermediate during routing. In our case, the `AuthenticationMiddleware` function is run for all requests before reaching the actual handlers to verify the user's token against Keycloak and then use it to authenticate with Vault and get a Vault Token. This middleware will also add more Headers to the request before forwarding it on to the actual handler. These headers provide the handler functions with information that was gleaned during the verification process such as username, admin status, and Vault token.


### Common Functions
There are a few common functions used by most handlers to process the incoming request, read the headers added by the middleware function, and read user information (clusters/secrets) from Vault.

Here are some of the important functions defined in `common.go`:
- `getWorkflowDefinitionData(request common.Request) (int, error)`
	- This function makes a request to the WorkflowDefinition Service and will parse the received information into the original `request` parameter
	- It returns an HTTP status code and error
- `prepareRequest(request common.Request, r *http.Request, workflowID string) (status int, err error)`
	- This function starts out by reading the `http.Request` body into our `common.Request` struct
	- Then, it will read and remove headers added by the middleware function to get the user's username, Vault token, and admin status
	- Finally, it will use the Vault token to read clusters and secrets from Vault if any secret or cluster IDs were included in the request
	- It returns an HTTP status code and error
- `checkAuthorization(request common.Request, r *http.Request, workflowID string) (int, error)`
	- This function just runs the previous two functions to first prepare the request and then get the WorkflowDefinition
	- This function was originally named this because it will fail if the user does not have access to the WorkflowDefinition, but it eventually evolved to do more than just that
	- It returns an HTTP status code and error
