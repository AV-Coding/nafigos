# Developer Documentation

### Table of Contents
[[_TOC_]]


### [API Service](./api-service.md)
### [Build Service](./build-service.md)
### [Keycloak setup](./keycloak.md)
### [Logging and Error Conventions in Nafigos](./logs_and_errors.md)
### [Phylax](./phylax.md)
### [Vault](./vault.md)
### [WorkflowDefinition Service](./workflow-definition-service.md)
