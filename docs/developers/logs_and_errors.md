# Logging and Error Conventions in Nafigos
This document will explain some of the decisions made for logging in Nafigos and will outline some of the requirements for adding new logs that are consistent with the rest of the project.

We use the [`logrus` package](https://github.com/Sirupsen/logrus) for logging. This package is really useful because it allows for all the regular logging statements that would be used by the built-in `log` package but also adds functionality like log levels, JSON (and other) formatting, and extra fields.


### Table of Contents
[[_TOC_]]


## Conventions/Rules
1. In general, do not output logs in functions that return errors. Instead, return a descriptive error that can be used by the consuming function in its logs

2. When returning errors, use a lower-case letter to start the message. This makes the error message easier to read when it is combined with other text and logged

3. Use `log.WithFields()` when logging and be sure to include: `package`, `function`, and `error` (where applicable)

4. Create a `logger` in functions with multiple logging opportunities to make it easier to log with common fields:
    ```go
    log "github.com/sirupsen/logrus"

    logger := log.WithFields(log.Fields{
            "package":  "packageName",
            "function": "functionName",
    })

    logger.Info("some info")

    logger.WithFields(log.Fields{"error": err}).Error("we had an error")
    ```


## Resources/Refernces
- https://github.com/Sirupsen/logrus
- https://www.datadoghq.com/blog/go-logging/
- https://blog.maddevs.io/how-to-start-with-logging-in-golang-projects-part-1-3e3a708b75be
- https://blog.golang.org/error-handling-and-go
- https://blog.golang.org/go1.13-errors
