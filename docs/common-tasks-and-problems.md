# Common Tasks and Problems


## Common Problems

### WorkflowDefinition won't build

**Problem:** My WorkflowDefinition won't build.  I get a response like:

```
{"error": {"code": 500, "message": "Error getting WorkflowDefinition 'bqln2k30ip2d3ojn820g': Error creating '' Workflow from string: no constructor for Workflow Type ''"}}
```

**Solution:**  This could be because Nafigos is unable to reach the Git repository for the Workflow.  Check to make sure that you have the correct URL for the repository and branch in your request.  If the repository is private, ensure that your user has the correct access tokens to reach the repository.
