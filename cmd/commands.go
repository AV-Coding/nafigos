package main

import (
	"github.com/mitchellh/cli"
	"gitlab.com/cyverse/nafigos/cmd/command"
)

// Commands holds a map of all possible commands
func Commands(meta *command.Meta) map[string]cli.CommandFactory {
	baseCommand := command.NewBaseCommand(*meta)
	return map[string]cli.CommandFactory{
		"create": func() (cli.Command, error) {
			return &command.CreateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"create user": func() (cli.Command, error) {
			return &command.CreateUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"create wfd": func() (cli.Command, error) {
			return &command.CreateWfdCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"create secret": func() (cli.Command, error) {
			return &command.CreateSecretCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"create kcluster": func() (cli.Command, error) {
			return &command.CreateKclusterCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"build": func() (cli.Command, error) {
			return &command.BuildCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"run": func() (cli.Command, error) {
			return &command.RunCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete": func() (cli.Command, error) {
			return &command.DeleteCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete user": func() (cli.Command, error) {
			return &command.DeleteUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete wfd": func() (cli.Command, error) {
			return &command.DeleteWfdCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete secret": func() (cli.Command, error) {
			return &command.DeleteSecretCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete kcluster": func() (cli.Command, error) {
			return &command.DeleteKclusterCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete run": func() (cli.Command, error) {
			return &command.DeleteRunCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get": func() (cli.Command, error) {
			return &command.GetCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get kcluster": func() (cli.Command, error) {
			return &command.GetKclusterCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get secret": func() (cli.Command, error) {
			return &command.GetSecretCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get user": func() (cli.Command, error) {
			return &command.GetUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get wfd": func() (cli.Command, error) {
			return &command.GetWfdCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get run": func() (cli.Command, error) {
			return &command.GetRunCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"login": func() (cli.Command, error) {
			return &command.LoginCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get build": func() (cli.Command, error) {
			return &command.GetBuildCommand{
				BaseCommand: &baseCommand,
				Meta:        *meta,
			}, nil
		},

		"version": func() (cli.Command, error) {
			return &command.VersionCommand{
				Version:  Version,
				Revision: GitCommit,
				Name:     Name,
			}, nil
		},
		"update": func() (cli.Command, error) {
			return &command.UpdateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"update kcluster": func() (cli.Command, error) {
			return &command.UpdateKclusterCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"update secret": func() (cli.Command, error) {
			return &command.UpdateSecretCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"update user": func() (cli.Command, error) {
			return &command.UpdateUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"update wfd": func() (cli.Command, error) {
			return &command.UpdateWfdCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
	}
}
