package command

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"encoding/json"
)

/*var tokenRes struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	IDToken     string `json:"id_token"`
	ExpiresIn   int64  `json:"expires_in"` // relative seconds from now
}*/

// BaseCommand ...
type BaseCommand struct {
	Meta
	nafigosAPI string
	authToken  string
}

// NewBaseCommand ...
func NewBaseCommand(meta Meta) (c BaseCommand) {
	c.Meta = meta
	c.authToken = os.Getenv("NAFIGOS_TOKEN")
	c.nafigosAPI = os.Getenv("NAFIGOS_API")
	// TODO: Fix this if string is empty
	if len(c.nafigosAPI) > 4 && c.nafigosAPI[0:4] != "http" {
		c.nafigosAPI = "http://" + c.nafigosAPI
	}
	return
}

// Client returns an HTTP Client to be used by the CLI
func (c *BaseCommand) Client() *http.Client {
	return &http.Client{}
}

// DoRequest will either execute a Request or print the curl command
func (c *BaseCommand) DoRequest(req *http.Request) {
	c.PrintDebug("Request struct:\n%v\n", req)
	if c.OutputCurlString {
		c.PrintCurlString(req)
	} else {
		resp, err := c.Client().Do(req)
		if err != nil {
			panic(err)
		}
		c.PrintDebug("Response struct:\n%v\n", resp)
		c.PrintHTTPResponse(resp)
	}
}

// SaveRequest will either execute a Request or print the curl command
func (c *BaseCommand) SaveRequest(req *http.Request) bool {
	c.PrintDebug("Request struct:\n%v\n", req)
	if c.OutputCurlString {
		c.PrintCurlString(req)
	} else {
		resp, err := c.Client().Do(req)
		if err != nil {
			panic(err)
		}
		c.PrintDebug("Response struct:\n%v\n", resp)
		return c.SaveResponse(resp)
	}
	return false
}

// NewRequest creates a new HTTP request with the correct header
func (c *BaseCommand) NewRequest(verb, path string, data string) *http.Request {
	req, err := http.NewRequest(verb, c.nafigosAPI+path, nil)
	if err != nil {
		panic(err)
	}
	if len(data) > 0 {
		req.Body = ioutil.NopCloser(strings.NewReader(data))
	}
	req.Header.Add("Authorization", c.authToken)
	return req
}

func isError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}

	return (err != nil)
}

// CheckConfiguration checks to see if the configuration file exists.
func (c *BaseCommand) CheckConfiguration() {
	var value string
	home := os.Getenv("HOME")
	_, err := os.Stat(home + "/.nafigos/config.json")
	if os.IsNotExist(err) {
		c.authToken = os.Getenv("NAFIGOS_TOKEN")
		return
	}
	jsonFile, _ := os.Open(home + "/.nafigos/config.json")
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	
	// Check to see if the length of the bytes is greater than '0'
	if len(byteValue) > 0{
		var result map[string]interface{}
		json.Unmarshal([]byte(byteValue), &result)
		value = result["id_token"].(string)

	}else {
		panic("No Credentials were found at ~/.nafigos/config.json")
	}
	c.authToken = value
	return
}