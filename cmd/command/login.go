package command

import (
	"flag"
	"bytes"
    "fmt"
    "io/ioutil"
	"strings"
	"strconv"
)

// LoginCommand ...
type LoginCommand struct {
	*BaseCommand
}

// Run ...
func (c *LoginCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	username := flagSet.String("username", "", "username")
	flagSet.Parse(args)

	c.CheckConfiguration()

	if len(*username) == 0 {
		c.UI.Error(c.Help())
		return 1
	}
	
	passwd, err := c.UI.AskSecret("Password:")
	if err != nil {
		panic(err)
	}

	req := c.NewRequest("POST", "/user/login", fmt.Sprintf("username=%s&password=%s", *username, passwd))
	
	loggedIn := c.SaveRequest(req)

	c.PrintDebug("loggenIn is : " + strconv.FormatBool(loggedIn))

	if loggedIn{
		c.CreateUser(*username)
		c.PrintDebug("Logged in Successfully to Keycloak!")
	}
	return 0
}

// Synopsis ...
func (c *LoginCommand) Synopsis() string {
	return "Allows for login to Keycloak"
}

// Help ...
func (c *LoginCommand) Help() string {
	helpText := `
	Usage: nafigos login [options]
	It is required that you use '-username <username>'

Options:
	-username <string>
		username associated with the secret (required)
`
	return strings.TrimSpace(helpText)
}

// CreateUser sets 'NAFIGOS_TOKEN` to id_token after 'nafigos login' command 
func (c *LoginCommand) CreateUser(username string){
	password :=""
	admin := false
	createKubeconfig := true
	data := []byte(fmt.Sprintf(`{
		"username": "%s",
		"password": "%s",
		"is_admin": %t,
		"create_kubeconfig": %t
	}`, username, password, admin, createKubeconfig))
	
	req := c.NewRequest("POST", "/users", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
}
