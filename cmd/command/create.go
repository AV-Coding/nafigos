package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// CreateCommand ...
type CreateCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateCommand) Run(args []string) int {
	c.CheckConfiguration()
	return cli.RunResultHelp
}

// Synopsis ...
func (c *CreateCommand) Synopsis() string {
	return "create a kcluster, secret, user or Workflow Definition "
}

// Help ...
func (c *CreateCommand) Help() string {
	helpText := `
	Usage: nafigos create <subcommand> [options]

`
	return strings.TrimSpace(helpText)
}
