package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/cyverse/nafigos/common"
)

// CreateUserCommand ...
type CreateUserCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateUserCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	username := flagSet.String("username", "", "username")
	password := flagSet.String("password", "", "password")
	admin := flagSet.Bool("admin", false, "admin")
	createKubeconfig := flagSet.Bool("create-kubeconfig", true, "create-kubeconfig")
	flagSet.Parse(args)

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  username: '%s'\n  password: '%s'\n  admin: %t\n  createKubeconfig: %t\n", *filename, *username, *password, *admin, *createKubeconfig)

	c.CheckConfiguration()
	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		if len(*username) > 0 || len(*password) > 0 || *admin || *createKubeconfig {
			data = c.override(data, username, password, admin, createKubeconfig)
			c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
		}
	} else {
		if len(*username) == 0 {
			c.UI.Error(c.Help())
			return 1
		}
		data = []byte(fmt.Sprintf(`{
	"username": "%s",
	"password": "%s",
	"is_admin": %t,
	"create_kubeconfig": %t
}`, *username, *password, *admin, *createKubeconfig))

		c.PrintDebug("Constructed request body:\n%s\n", string(data))
	}

	req := c.NewRequest("POST", "/users", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateUserCommand) Synopsis() string {
	return "create a new user"
}

// Help ...
func (c *CreateUserCommand) Help() string {
	helpText := `
	Usage: nafigos create user [options]

	Of the following options, it is required that you use either '-f <filename>' 
	or utilize at least '-username' and '-password'

	Options:
		-f <filename>
	  		name of json file to read user info from
		-username <string>
			(required if -f is not used)
		-password <string>
			(required if -f is not used)
		-admin
			give new user admin permissions
		-create-kubeconfig
			automatically generate a new kubeconfig to access the user cluster (default true)
	`
	return strings.TrimSpace(helpText)
}

func (c *CreateUserCommand) override(data []byte, username *string, password *string, admin *bool, createKubeconfig *bool) []byte {
	var userJSON struct {
		CreateKubeconfig bool `json:"create_kubeconfig"`
		common.User
	}
	json.Unmarshal(data, &userJSON)

	if len(*username) > 0 {
		userJSON.Username = *username
	}
	if len(*password) > 0 {
		userJSON.Password = *password
	}
	if *admin {
		userJSON.IsAdmin = true
	}
	if *createKubeconfig {
		userJSON.CreateKubeconfig = true
	}

	data, err := json.Marshal(userJSON)
	if err != nil {
		panic(err)
	}
	return data
}
