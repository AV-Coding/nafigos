package command

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

// BuildCommand ...
type BuildCommand struct {
	*BaseCommand
}

// Run ...
func (c *BuildCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	gitSecretID := flagSet.String("git-secret", "", "ID of secret to authenticate to git with")
	registrySecretID := flagSet.String("registry-secret", "", "ID of secret to authenticate to image registry with")
	clusterID := flagSet.String("cluster", "", "ID of cluster to build images on")
	namespace := flagSet.String("namespace", "", "namespace to use on cluster")
	run := flagSet.Bool("run", false, "whether to run this workflow after building")
	flagSet.Parse(args)
	args = flagSet.Args()

	c.CheckConfiguration()
	c.PrintDebug("Parsed command line flags:\n  gitSecretID: '%s'\n  registrySecretID: '%s'\n  clusterID: '%s'\n  namespace: '%s'\n run: %t\n", *gitSecretID, *registrySecretID, *clusterID, *namespace, *run)

	if len(args) < 1 {
		c.UI.Error("A workflow ID is required")
		c.UI.Error(c.Help())
		return 1
	}

	data := []byte(fmt.Sprintf(`{
	"registry_secret_id": "%s",
	"git_secret_id": "%s",
	"cluster_id": "%s",
	"namespace": "%s",
	"run": %t
}`, *registrySecretID, *gitSecretID, *clusterID, *namespace))

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("POST", fmt.Sprintf("/workflows/%s/builds", args[0]), "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *BuildCommand) Synopsis() string {
	return "build and push images from Workflow Definition"
}

// Help ...
func (c *BuildCommand) Help() string {
	helpText := `
Usage: nafigos build [options] ID

	Trigger the build and push of images from the Workflow Definition

Options:
	-cluster <string>
		ID of cluster to build on
	-namespace <string>
		namespace to use on cluster
	-registry-secret <string>
		ID of secret to use when authenticating with image registry
	-git-secret <string>
		ID of secret to use when authenticating with git (if private repo)
	-run
		run the associated Workflow immediately after successful build.
`
	return strings.TrimSpace(helpText)
}
