package command

import (
	"strings"
)

// GetWfdCommand ...
type GetWfdCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *GetWfdCommand) Run(args []string) int {
	c.CheckConfiguration()
	var wfdID string
	if len(args) > 0 {
		wfdID = "/" + args[0]
	}

	req := c.NewRequest("GET", "/workflows"+wfdID, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetWfdCommand) Synopsis() string {
	return "get WorkflowDefinitions"
}

// Help ...
func (c *GetWfdCommand) Help() string {
	helpText := `
Usage: nafigos get wfd [ID]

	Gets all wfds unless specific ID is provided
`
	return strings.TrimSpace(helpText)
}
