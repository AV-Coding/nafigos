package command

import (
	"strings"
)

// DeleteKclusterCommand ...
type DeleteKclusterCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteKclusterCommand) Run(args []string) int {
	c.CheckConfiguration()
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/kclusters/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteKclusterCommand) Synopsis() string {
	return "delete a kcluster"
}

// Help ...
func (c *DeleteKclusterCommand) Help() string {
	helpText := `
Usage: nafigos delete kcluster [kcluster ID]

Run 'nafigos get kcluster' to view kcluster ID'S			
`
	return strings.TrimSpace(helpText)
}
