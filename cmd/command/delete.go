package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// DeleteCommand ...
type DeleteCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteCommand) Run(args []string) int {
	c.CheckConfiguration()
	return cli.RunResultHelp
}

// Synopsis ...
func (c *DeleteCommand) Synopsis() string {
	return "delete a kcluster, secret, user or Work Flow Definition "
}

// Help ...
func (c *DeleteCommand) Help() string {
	helpText := `
	Usage: nafigos delete <subcommand>

`
	return strings.TrimSpace(helpText)
}
