package command

import (
	"strings"
)

// DeleteRunCommand ...
type DeleteRunCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteRunCommand) Run(args []string) int {
	c.CheckConfiguration()
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/runs/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteRunCommand) Synopsis() string {
	return "delete a run by [Runner ID]"
}

// Help ...
func (c *DeleteRunCommand) Help() string {
	helpText := `
Usage: nafigos delete run [Runner ID]

Run 'nafigos get run' to view run ID'S
`
	return strings.TrimSpace(helpText)
}
