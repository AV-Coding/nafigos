package command

import (
	"strings"
)

// DeleteUserCommand ...
type DeleteUserCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteUserCommand) Run(args []string) int {
	c.CheckConfiguration()
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/users/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteUserCommand) Synopsis() string {
	return "delete a user"
}

// Help ...
func (c *DeleteUserCommand) Help() string {
	helpText := `
Usage: nafigos delete user [USERNAME]

Run 'nafigos get user' to view list of users.
`
	return strings.TrimSpace(helpText)
}
