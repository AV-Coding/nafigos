package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/cyverse/nafigos/workflow-definition-service/workflowdefinition"
)

// UpdateWfdCommand ...
type UpdateWfdCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateWfdCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	gitSecretID := flagSet.String("git-secret", "", "ID of secret to authenticate to git with")
	// WFD Fields:
	name := flagSet.String("name", "", "new name")
	description := flagSet.String("description", "", "new description")
	wfdType := flagSet.String("type", "", "new Workflow Type")
	build := flagSet.String("build", "", "new Build instructions")
	tcpPorts := flagSet.String("tcp-ports", "", "new TCP Ports")
	httpPorts := flagSet.String("http-ports", "", "new HTTP Ports")
	workflow := flagSet.String("workflow", "", "new Workflow")
	// Now parse WFD args
	flagSet.Parse(args)
	args = flagSet.Args()

	c.CheckConfiguration()
	c.PrintDebug(`Parsed command line flags:
  filename: '%s'
  gitSecretID: '%s'
  name: '%s'
  description: '%s'
  wfdType: '%s'
  build: '%s'
  tcpPorts: '%s'
  httpPorts: '%s'
  workflow: '%s'
`, *filename, *gitSecretID, *name, *description, *wfdType, *build, *tcpPorts, *httpPorts, *workflow)

	if len(args) == 1 && args[0] == "help" {
		c.UI.Error(c.Help())
		return 0
	}

	if len(args) != 1 {
		c.UI.Error("Incorrect number of positional arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	workflowID := args[0]

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		data = c.override(data, *name, *description, *wfdType, *build, *tcpPorts, *httpPorts, *workflow, *gitSecretID)
		c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
	} else {
		// Only add the fields that are included in the command so that we can either edit
		// or update from git in the API
		dataString := ""
		if len(*gitSecretID) > 0 {
			dataString += fmt.Sprintf(`, "git_secret_id": "%s"`, *gitSecretID)
		}
		if len(*name) > 0 {
			dataString += fmt.Sprintf(`, "name": "%s"`, *name)
		}
		if len(*description) > 0 {
			dataString += fmt.Sprintf(`, "description": "%s"`, *description)
		}
		if len(*wfdType) > 0 {
			dataString += fmt.Sprintf(`, "type": "%s"`, *wfdType)
		}
		if len(*workflow) > 0 {
			dataString += fmt.Sprintf(`, "workflow": "%s"`, *workflow)
		}

		// The following fields are not intended to be strings, but are read as strings from the
		// command line args, so they must be conditionally included (and not surrounded by quotes)
		if len(*build) > 0 {
			dataString += ", \"build\": " + *build
		}
		if len(*tcpPorts) > 0 {
			dataString += ", \"tcp_ports\": " + *tcpPorts
		}
		if len(*httpPorts) > 0 {
			dataString += ", \"http_ports\": " + *httpPorts
		}

		if len(dataString) > 0 {
			dataString = "{" + dataString[2:] + "}"
		}

		data = []byte(dataString)
		c.PrintDebug("Constructed request body:\n%s\n", string(data))
	}

	req := c.NewRequest("PUT", "/workflows/"+workflowID, "")

	// Only fill request body if it is not empty
	if len(data) > 0 {
		req.Body = ioutil.NopCloser(bytes.NewReader(data))
	}
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateWfdCommand) Synopsis() string {
	return "update a Workflow Definition"
}

// Help ...
func (c *UpdateWfdCommand) Help() string {
	helpText := `
	Usage: nafigos update wfd [options] [ID]

	Options:
		-f <filename>
			name of JSON file to read info from
		-git-secret <git_secret_id>
			ID of the Git secret to use to push to repository
	WorkflowDefinition fields:
		-name
			Name of the Workflow
		-description
			Description of the Workflow
		-type
			Type of the Workflow
		-build
			Build instruction of WFD. Example:
				'[{"dockerfile": "Dockerfile", "image": "my-image"}]'
		-tcp-ports
			TCP Ports to expose in Workflow. Example:
				'[{"name": "my-port", "port": 1234, "targetPort": 1234}]'
		-http-ports
			HTTP Ports to expose in Workflow. Example:
				'[{"name": "my-port", "port": 1234, "targetPort": 1234}]'
		-workflow
			The actual Workflow contents. Must be a string representing the
			selected Workflow Type
`
	return strings.TrimSpace(helpText)
}

func (c *UpdateWfdCommand) override(data []byte, name, description, wfdType, build, tcpPorts, httpPorts, workflow, gitSecretID string) []byte {
	var wfdJSON struct {
		workflowdefinition.WorkflowDefinition
		GitSecretID string `json:"git_secret_id"`
	}
	json.Unmarshal(data, &wfdJSON)

	if len(name) > 0 {
		wfdJSON.Name = name
	}
	if len(description) > 0 {
		wfdJSON.Description = description
	}
	if len(wfdType) > 0 {
		wfdJSON.Type = wfdType
	}
	if len(build) > 0 {
		err := json.Unmarshal([]byte(build), wfdJSON.Build)
		if err != nil {
			c.UI.Error("Invalid Build field: " + err.Error())
		}
	}
	if len(tcpPorts) > 0 {
		err := json.Unmarshal([]byte(tcpPorts), wfdJSON.TCPPorts)
		if err != nil {
			c.UI.Error("Invalid TCPPorts field: " + err.Error())
		}
	}
	if len(httpPorts) > 0 {
		err := json.Unmarshal([]byte(httpPorts), wfdJSON.HTTPPorts)
		if err != nil {
			c.UI.Error("Invalid HTTPPorts field: " + err.Error())
		}
	}
	if len(workflow) > 0 {
		wfdJSON.RawWorkflow = workflow
	}
	if len(gitSecretID) > 0 {
		wfdJSON.GitSecretID = gitSecretID
	}

	data, err := json.Marshal(wfdJSON)
	if err != nil {
		panic(err)
	}
	return data
}
