package command

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

// RunCommand ...
type RunCommand struct {
	*BaseCommand
}

// Run ...
func (c *RunCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	clusterID := flagSet.String("cluster", "", "ID of cluster to build images on")
	namespace := flagSet.String("namespace", "", "namespace to use on cluster")
	authDisable := flagSet.Bool("disable-auth", false, "disable auth requirement for running WFD")
	flagSet.Parse(args)
	args = flagSet.Args()

	c.CheckConfiguration()
	c.PrintDebug("Parsed command line flags:\n  clusterID: '%s'\n  namespace: '%s'\n  authDisable: %t\n", *clusterID, *namespace, *authDisable)

	if len(args) < 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	data := []byte(fmt.Sprintf(`{
	"workflow_id": "%s",
	"cluster_id": "%s",
	"namespace": "%s",
	"auth_disabled": %t
}`, args[0], *clusterID, *namespace, *authDisable))

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("POST", "/runs", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *RunCommand) Synopsis() string {
	return "run a workflow"
}

// Help ...
func (c *RunCommand) Help() string {
	helpText := `
	Usage: nafigos run [options] <WFD ID>

		Run a WorkflowDefinition

	Options:
		-kcluster <string>
			ID of cluster to run on
		-namespace <string>
			namespace to use on cluster
`
	return strings.TrimSpace(helpText)
}
