package command

import (
	"strings"
)

// DeleteWfdCommand ...
type DeleteWfdCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteWfdCommand) Run(args []string) int {
	c.CheckConfiguration()
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/workflows/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteWfdCommand) Synopsis() string {
	return "delete a WorkflowDefinition"
}

// Help ...
func (c *DeleteWfdCommand) Help() string {
	helpText := `
Usage: nafigos delete wfd [workflow ID]

Run 'nafigos get wfd' to view list of Work Flow Definitions.
`
	return strings.TrimSpace(helpText)
}
