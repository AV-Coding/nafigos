package command

import (
	"strings"
)

// GetBuildCommand ...
type GetBuildCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *GetBuildCommand) Run(args []string) int {
	
	c.CheckConfiguration()
	var buildID string
	if len(args) > 0 {
		buildID = "/" + args[0]
	}

	req := c.NewRequest("GET", "/builds"+buildID, "")
	resp, err := c.Client().Do(req)
	if err != nil {
		panic(err)
	}
	c.PrintHTTPResponse(resp)
	return 0
}

// Synopsis ...
func (c *GetBuildCommand) Synopsis() string {
	return "Get a build by ID.  Returns all builds for the current user if no ID is specified"
}

// Help ...
func (c *GetBuildCommand) Help() string {
	helpText := `
Usage: nafigos get build [BUILDID]

	Returns all builds if no build ID is specified
`
	return strings.TrimSpace(helpText)
}
