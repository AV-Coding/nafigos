package command

import (
	"strings"
)

// GetKclusterCommand ...
type GetKclusterCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetKclusterCommand) Run(args []string) int {
	c.CheckConfiguration()
	var kclusterID string
	if len(args) > 0 {
		kclusterID = "/" + args[0]
	}

	req := c.NewRequest("GET", "/kclusters"+kclusterID, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetKclusterCommand) Synopsis() string {
	return "get kclusters"
}

// Help ...
func (c *GetKclusterCommand) Help() string {
	helpText := `
Usage: nafigos get kcluster [ID]

	Gets all kclusters unless specific ID is provided
`
	return strings.TrimSpace(helpText)
}
