package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"gitlab.com/cyverse/nafigos/phylax-service/phylax"

	"github.com/stretchr/testify/assert"
)

var buildID string

var tests03Build = []testMap{
	{"StartBuildBadWorkflow": testStartBuildBadWorkflow},
	{"StartBuildNoRegistrySecret": testStartBuildNoRegistrySecret},
	{"StartBuildSecretNotFound": testStartBuildSecretNotFound},
	{"StartBuildWFDPermissionDenied": testStartBuildWFDPermissionDenied},
	{"StartBuildBadSecretFail": testStartBuildBadSecretFail},
	{"GetAllBuilds": testGetAllBuilds},
}

func testStartBuildBadWorkflow(t *testing.T) {
	data := fmt.Sprintf(`{"workflow_id": "%s", "cluster_id": "%s", "registry_secret_id": "%s"}`, errorWorkflowID, generatedCluster.ID, secretID)
	req, err := newRequest("POST", "http://localhost/workflows/"+workflowID+"/builds", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, `{"error": {"code": 400, "message": "Unable to build WorkflowDefinition in an error state. Please fix Workflow and try again"}}`, string(respBody))
}

func testStartBuildNoRegistrySecret(t *testing.T) {
	data := fmt.Sprintf(`{"cluster_id": "%s"}`, generatedCluster.ID)
	req, err := newRequest("POST", "http://localhost/workflows/"+workflowID+"/builds", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.NoError(t, err)
	assert.Equal(t, `{"error": {"code": 400, "message": "required registry secret ID was not provided"}}`, string(respBody))
}

func testStartBuildSecretNotFound(t *testing.T) {
	data := fmt.Sprintf(`{"registry_secret_id": "abcdefghijklmnopqrst", "cluster_id": "%s"}`, generatedCluster.ID)
	req, err := newRequest("POST", "http://localhost/workflows/"+workflowID+"/builds", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.NoError(t, err)
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading container registry secret 'abcdefghijklmnopqrst': secret 'abcdefghijklmnopqrst' not found"}}`, string(respBody))
}

func testStartBuildWFDPermissionDenied(t *testing.T) {
	data := fmt.Sprintf(`{"registry_secret_id": "%s", "cluster_id": "%s"}`, secretID, generatedCluster.ID)
	req, err := newRequest("POST", "http://localhost/workflows/"+adminWorkflowID+"/builds", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusInternalServerError, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.NoError(t, err)
	assert.Equal(t, fmt.Sprintf(`{"error": {"code": 500, "message": "unable to get WorkflowDefinition data: unable to get WorkflowDefinition '%s': user not authorized for this WorkflowDefinition"}}`, adminWorkflowID), string(respBody))
}

func testStartBuildBadSecretFail(t *testing.T) {
	data := fmt.Sprintf(`{"registry_secret_id": "%s", "cluster_id": "%s"}`, secretID, generatedCluster.ID)
	req, err := newRequest("POST", "http://localhost/workflows/"+workflowID+"/builds", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result map[string]string
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	buildID = result["id"]
	assert.Len(t, buildID, 20)
	time.Sleep(30 * time.Second)

	var build struct {
		Build *phylax.Build `json:"build"`
	}

	i := 0
	for i < 10 {
		req, err = newRequest("GET", "http://localhost/builds/"+buildID, "", userToken)
		assert.NoError(t, err)
		resp, err = (&http.Client{}).Do(req)
		assert.NoError(t, err)
		assert.Equal(t, http.StatusOK, resp.StatusCode)
		respBody, err = ioutil.ReadAll(resp.Body)
		assert.NoError(t, err)
		assert.Greater(t, len(respBody), 0)
		err = json.Unmarshal(respBody, &build)
		assert.NoError(t, err)
		if build.Build.Condition == phylax.BuildPending {
			fmt.Println("Build is pending, sleeping for 20 seconds")
			time.Sleep(20 * time.Second)
			i = i + 1
		} else {
			break
		}
	}

	assert.Equal(t, phylax.BuildFailed, build.Build.Condition)
}

func testGetAllBuilds(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/builds", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result struct {
		Builds []phylax.Build `json:"builds"`
	}
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	// Even though we try to Build multiple times, the only one that makes it
	// to Phylax and is stored is the one with the bad registry secret.  This
	// is because we don't know that the secret is bad until we try to build it.
	assert.Equal(t, 1, len(result.Builds))
	assert.Equal(t, phylax.BuildFailed, result.Builds[0].Condition)
}
