package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/stretchr/testify/assert"
)

var secretID string

var tests00User = []testMap{
	{"LoginAsAdmin": testLoginAsAdmin},
	{"CreateAdminUser": testCreateAdminUser},
	{"CreateRegularUser": testCreateRegularUser},
	{"LoginAsUser": testLoginAsUser},
	{"GetUser": testGetUser},
	{"GetUserPermissionDenied": testGetUserPermissionDenied},
	{"GetUserNotFound": testGetUserNotFound},
	{"GetNonExistentUserPermissionDenied": testGetNonExistentUserPermissionDenied},
	{"GetAllUsersAdmin": testGetAllUsersAdmin},
	{"GetAllUsersRegular": testGetAllUsersRegular},
	{"CreateSecret": testCreateSecret},
	{"GetSecret": testGetSecret},
	{"GetAllSecrets": testGetAllSecrets},
	{"GetAllSecretsPermissionDenied": testGetAllSecretsPermissionDenied},
	{"UpdateSecret": testUpdateSecret},
	{"DeleteSecret": testDeleteSecret},
	{"GetSecretNotFound": testGetSecretNotFound},
	{"CreateRegistrySecret": testCreateRegistrySecret},
	{"MakeSelfAdminFails": testMakeSelfAdminFails},
	{"DeleteUserPermissionDenied": testDeleteUserPermissionDenied},
}

func testLoginAsAdmin(t *testing.T) {
	data := `username=nafigos-admin&password=nafigos-admin-password`
	req, err := newRequest("POST", "http://localhost/user/login", data, "")
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result struct {
		IDToken string `json:"id_token"`
	}
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Greater(t, len(result.IDToken), 0)
	adminToken = result.IDToken
}

func testCreateAdminUser(t *testing.T) {
	data := `{
	"username": "nafigos-admin",
	"password": "nafigos-admin-password",
	"is_admin": true,
	"create_kubeconfig": false,
	"secrets": {
		"1": {
			"value": "none",
			"username": "none",
			"type": "git"
		}
	},
	"clusters": {
		"1": {
			"config": "none",
			"default_namespace": "none",
			"name": "none",
			"host": "none"
		}
	}
}`
	req, err := newRequest("POST", "http://localhost/users", data, adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
}

func testCreateRegularUser(t *testing.T) {
	data := `{
	"username": "nafigos-user",
	"password": "nafigos-user-password",
	"is_admin": false,
	"create_kubeconfig": true
}`
	req, err := newRequest("POST", "http://localhost/users", data, adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	time.Sleep(5 * time.Second)
}

func testLoginAsUser(t *testing.T) {
	data := `username=nafigos-user&password=nafigos-user-password`
	req, err := newRequest("POST", "http://localhost/user/login", data, "")
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result struct {
		IDToken string `json:"id_token"`
	}
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Greater(t, len(result.IDToken), 0)
	userToken = result.IDToken
}

func testGetUser(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/users/nafigos-user", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var user common.User
	err = json.Unmarshal(respBody, &user)
	assert.NoError(t, err)
	assert.Equal(t, "nafigos-user", user.Username)
	assert.Empty(t, user.Password)
	assert.Empty(t, user.Secrets)
	assert.NotEmpty(t, user.Clusters)
	assert.False(t, user.IsAdmin)

	// Save this generated cluster for use later
	for k := range user.Clusters {
		generatedCluster = user.Clusters[k]
	}
}

func testGetUserPermissionDenied(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/users/nafigos-admin", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusForbidden, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to get user: non-admin user 'nafigos-user' cannot get other user 'nafigos-admin'"}}`, string(respBody))
}

func testGetUserNotFound(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/users/nonexistent-user", "", adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNotFound, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, `{"error": {"code": 404, "message": "unable to get user: user 'nonexistent-user' not found"}}`, string(respBody))
}

func testGetNonExistentUserPermissionDenied(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/users/nonexistent-user", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusForbidden, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to get user: non-admin user 'nafigos-user' cannot get other user 'nonexistent-user'"}}`, string(respBody))
}

func testGetAllUsersAdmin(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/users", "", adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Contains(t, []string{
		`["nafigos-user","nafigos-admin"]`,
		`["nafigos-admin","nafigos-user"]`,
	}, string(respBody))
}

func testGetAllUsersRegular(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/users", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, `["nafigos-user"]`, string(respBody))
}

func testCreateSecret(t *testing.T) {
	data := `{
	"username": "nafigos-user",
	"type": "git",
	"value": "secret"
}`
	req, err := newRequest("POST", "http://localhost/users/nafigos-user/secrets", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var secret common.Secret
	err = json.Unmarshal(respBody, &secret)
	assert.NoError(t, err)
	secretID = secret.ID
	assert.Len(t, secretID, 20)
	assert.Equal(t, "nafigos-user", secret.Username)
	assert.Equal(t, common.SecretType("git"), secret.Type)
	assert.Equal(t, "secret", secret.Value)
}

func testGetSecret(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/users/nafigos-user/secrets/"+secretID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var secret common.Secret
	err = json.Unmarshal(respBody, &secret)
	assert.NoError(t, err)
	assert.Equal(t, "nafigos-user", secret.Username)
	assert.Equal(t, common.SecretType("git"), secret.Type)
	assert.Equal(t, "secret", secret.Value)
	assert.Equal(t, secretID, secret.ID)
}

func testGetAllSecrets(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/users/nafigos-user/secrets", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var secrets map[string]common.Secret
	err = json.Unmarshal(respBody, &secrets)
	assert.NoError(t, err)
	assert.Equal(t, 1, len(secrets))
	assert.NotEmpty(t, secrets[secretID])
}

func testGetAllSecretsPermissionDenied(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/users/nafigos-admin/secrets", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusForbidden, resp.StatusCode)
}

func testUpdateSecret(t *testing.T) {
	data := `{
		"username": "new-user",
		"type": "git",
		"value": "new-secret"
	}`
	req, err := newRequest("PUT", "http://localhost/users/nafigos-user/secrets/"+secretID, data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)

	req, err = newRequest("GET", "http://localhost/users/nafigos-user/secrets/"+secretID, "", userToken)
	assert.NoError(t, err)
	resp, err = (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var secret common.Secret
	err = json.Unmarshal(respBody, &secret)
	assert.NoError(t, err)
	assert.Equal(t, "new-user", secret.Username)
	assert.Equal(t, common.SecretType("git"), secret.Type)
	assert.Equal(t, "new-secret", secret.Value)
	assert.Equal(t, secretID, secret.ID)
}

func testDeleteSecret(t *testing.T) {
	req, err := newRequest("DELETE", "http://localhost/users/nafigos-user/secrets/"+secretID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNoContent, resp.StatusCode)
}

func testGetSecretNotFound(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/users/nafigos-user/secrets/"+secretID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, fmt.Sprintf(`{"error": {"code": 400, "message": "unable to get secret: secret '%s' not found"}}`, secretID), string(respBody))
}

func testCreateRegistrySecret(t *testing.T) {
	data := `{
		"username": "nafigos-user",
		"type": "dockerhub",
		"value": "secret"
	}`
	req, err := newRequest("POST", "http://localhost/users/nafigos-user/secrets", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var secret common.Secret
	err = json.Unmarshal(respBody, &secret)
	assert.NoError(t, err)
	secretID = secret.ID
	assert.Len(t, secretID, 20)
	assert.Equal(t, "nafigos-user", secret.Username)
	assert.Equal(t, common.SecretType("dockerhub"), secret.Type)
	assert.Equal(t, "secret", secret.Value)
}

func testMakeSelfAdminFails(t *testing.T) {
	data := `{"is_admin": true}`
	req, err := newRequest("PUT", "http://localhost/users/nafigos-user", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusForbidden, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to write user: 1 error occurred:
permission denied"}}`, string(respBody))
}

func testDeleteUserPermissionDenied(t *testing.T) {
	req, err := newRequest("DELETE", "http://localhost/users/nafigos-user", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusForbidden, resp.StatusCode)
}

func testDeleteUser(t *testing.T) {
	req, err := newRequest("DELETE", "http://localhost/users/nafigos-user", "", adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNoContent, resp.StatusCode)
}

func testDeleteAdminUser(t *testing.T) {
	req, err := newRequest("DELETE", "http://localhost/users/nafigos-admin", "", adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNoContent, resp.StatusCode)
}
