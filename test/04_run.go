package test

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/phylax-service/phylax"

	"github.com/stretchr/testify/assert"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"
)

var runID, resourceName string

var tests04Run = []testMap{
	{"StartRun": testStartRun},
	{"StartRunBadWorkflow": testStartRunBadWorkflow},
	{"GetRunner": testGetRunner},
	{"AccessRunningService": testAccessRunningService},
	{"AccessRunningServiceNoAuthEnforced": testAccessRunningServiceNoAuthEnforced},
	{"AccessRunningServicePermissionDenied": testAccessRunningServicePermissionDenied},
	{"StartRunNoAuth": testStartRunNoAuth},
	{"AccessRunningServiceNoAuthSuccess": testAccessRunningServiceNoAuthSuccess},
	{"GetAllClusterResources": testGetAllClusterResources},
	{"GetAllClusterResourcesInvalidNamespace": testGetAllClusterResourcesInvalidNamespace},
	// DeleteRun is used here since this run will create resources in the `istio-system` namespace
	// that will not be deleted when the user namespace is deleted.
	{"DeleteRun": testDeleteRun},
	{"StartRunNewPorts": testStartRunNewPorts},
	{"AccessRunningServiceNewPorts": testAccessRunningServiceNewPorts},
	{"GetAllRunners": testGetAllRunners},
}

func testStartRun(t *testing.T) {
	data := fmt.Sprintf(`{"workflow_id": "%s", "cluster_id": "%s"}`, workflowID, generatedCluster.ID)
	req, err := newRequest("POST", "http://localhost/runs", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result map[string]string
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	runID = result["id"]
	assert.Len(t, runID, 20)
	resourceName = common.CreateResourceName("nafigos-user", "nafigos-helloworld", runID)
}

func testStartRunBadWorkflow(t *testing.T) {
	data := fmt.Sprintf(`{"workflow_id": "%s", "cluster_id": "%s"}`, errorWorkflowID, generatedCluster.ID)
	req, err := newRequest("POST", "http://localhost/runs", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, `{"error": {"code": 400, "message": "Unable to run WorkflowDefinition in an error state. Please fix Workflow and try again"}}`, string(respBody))
}

func testGetRunner(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/runs/"+runID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result struct {
		Runner *phylax.Runner `json:"runner"`
	}
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, runID, result.Runner.ID)
	assert.Equal(t, resourceName, result.Runner.ResourceName)
}

func testAccessRunningService(t *testing.T) {
	// First, wait for pod to be ready
	kubeconfig, err := base64.StdEncoding.DecodeString(generatedCluster.Config)
	assert.NoError(t, err)
	config, err := clientcmd.RESTConfigFromKubeConfig(kubeconfig)
	assert.NoError(t, err)
	clientset, err := kubernetes.NewForConfig(config)
	assert.NoError(t, err)

	stop := make(chan struct{})
	optionsModifier := func(options *meta.ListOptions) {
		options.LabelSelector = "app=" + resourceName
	}
	watchList := cache.NewFilteredListWatchFromClient(clientset.CoreV1().RESTClient(), "pods", "nafigos-user", optionsModifier)
	_, controller := cache.NewInformer(watchList, &core.Pod{}, time.Second*0, cache.ResourceEventHandlerFuncs{
		UpdateFunc: func(oldObj, newObj interface{}) {
			if newObj.(*core.Pod).Status.Phase == "Running" {
				close(stop)
			}
		},
	})
	controller.Run(stop)

	time.Sleep(500 * time.Millisecond)

	// Now make request to the running Workflow
	req, err := newRequest("GET", "http://localhost:8080/"+runID+":81", "", userToken)
	assert.NoError(t, err)
	req.Host = generatedCluster.Host
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, "Hello nafigos!", string(respBody))
}

func testAccessRunningServiceNoAuthEnforced(t *testing.T) {
	// Now make request to the running Workflow
	req, err := http.NewRequest("GET", "http://localhost:8080/"+runID+":81", nil)
	assert.NoError(t, err)
	req.Host = generatedCluster.Host
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusUnauthorized, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, "Origin authentication failed.", string(respBody))
}

func testAccessRunningServicePermissionDenied(t *testing.T) {
	// Now make request to the running Workflow
	req, err := newRequest("GET", "http://localhost:8080/"+runID+":81", "", adminToken)
	assert.NoError(t, err)
	req.Host = generatedCluster.Host
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusForbidden, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, "RBAC: access denied", string(respBody))
}

func testDeleteRun(t *testing.T) {
	req, err := newRequest("DELETE", "http://localhost/runs/"+runID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNoContent, resp.StatusCode)
}

func testStartRunNoAuth(t *testing.T) {
	data := fmt.Sprintf(`{"workflow_id": "%s", "cluster_id": "%s", "auth_disabled": true}`, workflowID, generatedCluster.ID)
	req, err := newRequest("POST", "http://localhost/runs", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result map[string]string
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	runID = result["id"]
	assert.Len(t, runID, 20)
	resourceName = common.CreateResourceName("nafigos-user", "nafigos-helloworld", runID)
}

func testAccessRunningServiceNoAuthSuccess(t *testing.T) {
	// First, wait for pod to be ready
	kubeconfig, err := base64.StdEncoding.DecodeString(generatedCluster.Config)
	assert.NoError(t, err)
	config, err := clientcmd.RESTConfigFromKubeConfig(kubeconfig)
	assert.NoError(t, err)
	clientset, err := kubernetes.NewForConfig(config)
	assert.NoError(t, err)

	stop := make(chan struct{})
	optionsModifier := func(options *meta.ListOptions) {
		options.LabelSelector = "app=" + resourceName
	}
	watchList := cache.NewFilteredListWatchFromClient(clientset.CoreV1().RESTClient(), "pods", "nafigos-user", optionsModifier)
	_, controller := cache.NewInformer(watchList, &core.Pod{}, time.Second*0, cache.ResourceEventHandlerFuncs{
		UpdateFunc: func(oldObj, newObj interface{}) {
			if newObj.(*core.Pod).Status.Phase == "Running" {
				close(stop)
			}
		},
	})
	controller.Run(stop)

	time.Sleep(45 * time.Second)

	// Now make request to the running Workflow
	req, err := http.NewRequest("GET", "http://localhost:8080/"+runID+":81", nil)
	assert.NoError(t, err)
	req.Host = generatedCluster.Host
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, "Hello nafigos!", string(respBody))
}

func testGetAllClusterResources(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/kclusters/"+generatedCluster.ID+"/resources", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result struct {
		Pods          []map[string]interface{} `json:"pods"`
		Deployments   []map[string]interface{} `json:"deployments"`
		Jobs          []map[string]interface{} `json:"jobs"`
		ArgoWorkflows []map[string]interface{} `json:"argo_workflows"`
	}
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, 0, len(result.ArgoWorkflows))
	assert.Equal(t, 1, len(result.Jobs))
	assert.Equal(t, 2, len(result.Deployments))
	assert.Equal(t, 2, len(result.Pods))
}

func testGetAllClusterResourcesInvalidNamespace(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/kclusters/"+generatedCluster.ID+"/resources?namespace=test", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusInternalServerError, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result struct {
		common.Error `json:"error"`
	}
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusInternalServerError, result.Code)
	assert.Equal(t, `unable to get pod list: pods is forbidden: User "nafigos-user" cannot list resource "pods" in API group "" in the namespace "test"`, result.Message)
}

func testStartRunNewPorts(t *testing.T) {
	data := fmt.Sprintf(`{
	"workflow_id": "%s",
	"cluster_id": "%s",
	"http_ports": [{"name": "frontend", "port": 82, "targetPort": "frontend"}]
}`, workflowID, generatedCluster.ID)
	req, err := newRequest("POST", "http://localhost/runs", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result map[string]string
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	runID = result["id"]
	assert.Len(t, runID, 20)
	resourceName = common.CreateResourceName("nafigos-user", "nafigos-helloworld", runID)
}

func testAccessRunningServiceNewPorts(t *testing.T) {
	// First, wait for pod to be ready
	kubeconfig, err := base64.StdEncoding.DecodeString(generatedCluster.Config)
	assert.NoError(t, err)
	config, err := clientcmd.RESTConfigFromKubeConfig(kubeconfig)
	assert.NoError(t, err)
	clientset, err := kubernetes.NewForConfig(config)
	assert.NoError(t, err)

	stop := make(chan struct{})
	optionsModifier := func(options *meta.ListOptions) {
		options.LabelSelector = "app=" + resourceName
	}
	watchList := cache.NewFilteredListWatchFromClient(clientset.CoreV1().RESTClient(), "pods", "nafigos-user", optionsModifier)
	_, controller := cache.NewInformer(watchList, &core.Pod{}, time.Second*0, cache.ResourceEventHandlerFuncs{
		UpdateFunc: func(oldObj, newObj interface{}) {
			if newObj.(*core.Pod).Status.Phase == "Running" {
				close(stop)
			}
		},
	})
	controller.Run(stop)

	time.Sleep(500 * time.Millisecond)

	// Now make request to the running Workflow
	req, err := newRequest("GET", "http://localhost:8080/"+runID+":82", "", userToken)
	assert.NoError(t, err)
	req.Host = generatedCluster.Host
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, "Hello nafigos!", string(respBody))
}

func testGetAllRunners(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/runs", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result struct {
		Runners []phylax.Runner `json:"runners"`
	}
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, 3, len(result.Runners))
}
