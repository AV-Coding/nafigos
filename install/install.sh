#!/bin/bash

if ! dpkg -s ansible > /dev/null 22>&1 ; then 

    sudo apt update
    sudo apt install software-properties-common
    sudo apt-add-repository --yes --update ppa:ansible/ansible
    sudo apt install ansible=2.8.5-1ppa~bionic --yes

fi

if [ ! -f config.yml ] ; then 
    echo "missing config.yml"
    exit 1
fi

ansible-playbook playbook.yml

