# Install

### Table of Contents
[[_TOC_]]


## Getting Started

This directory is reserved for ansible and other resources needed to deploy the Nafigos platform and user clusters.

There are three playbooks:

* `playbook-kubespray.yml` - used to install a multinode kubernetes cluster using kubespray
* `playbook-nafigos.yml` - used to install nafigos, and depending on the values in config.yml, it will also setup a development environment (currently using kind)
* `playbook-vault.yml` - used to install Vault on a separate server with PostgreSQL storage backend

Note: you should ensure that you have ansible (and ansible-galaxy) installed on the system that you'll run ansible on.

1. Install `requirements.yml`: If you are setting up a new development environment, you should install dependencies in `requirements.yml` from the host that will be running ansible
    ```bash
    ansible-galaxy role install -r requirements.yml
    ```

2. For remote deployments, copy `example_hosts.yml` to `hosts.yml`
    ```bash
    cp example_hosts.yml hosts.yml
    ```

## Nafigos Install

1. `config.yml`: copy `example_config.yml` to `config.yml` if it's a new deployment; edit `config.yml` as necessary
    ```bash
    cp example_config.yml config.yml
    ```
    For local development, there are a few important variables to change from the defaults in `config.yml`:
    ```diff
    - MONGODB_CONTAINER_ENABLED: false
    - SKAFFOLD: false
    + MONGODB_CONTAINER_ENABLED: true
    + SKAFFOLD: true

    - NAFIGOS_GIT_REPO:  https://gitlab.com/cyverse/nafigos.git
    - NAFIGOS_GIT_BRANCH: master
    - NAFIGOS_SRC_DIR: /opt/nafigos
    + NAFIGOS_GIT_REPO:  https://gitlab.com/<your_gitlab_username>/nafigos.git # and use SSH link if you prefer
    + NAFIGOS_GIT_BRANCH: <your_current_branch>
    + NAFIGOS_SRC_DIR: <a_user_accessible_directory> # this is wherever you cloned the nafigos repo
    ```

2. To start the nafigos install process for remote hosts, execute the ansible-playbook
    ```bash
    ansible-playbook -i hosts.yml playbook-nafigos.yml
    ```

    For local development, use `localhost.yml`
    ```bash
    ansible-playbook -K -i localhost.yml playbook-nafigos.yml
    ```

## Vault Install

The `playbook-vault.yml` playbook is intended to be used separate from the other Nafigos playbooks. This is used to install Vault on a separate server (not a K8s cluster) alongside a PostgreSQL backend. The PostgreSQL database must be installed and setup separately, but this playbook will create the necessary tables required by Vault. Once the playbook completes, Vault will be installed, running, and unsealed. The unseal keys and root token are saved in `vault_secrets.json`.

1. Make sure to setup the `vault_host` in `hosts.yml`

2. Copy the default Vault config file:
    ```bash
    cp example_config_vault.yml vault_config.yml
    ```

3. Edit the variables, making sure to use the correct secure password for PostgreSQL
    - If you require TLS for Vault (**which should be used for production**), make sure to enable it and include cert and key data like this:
        ```yaml
        VAULT_TLS_KEYDATA: |-
          -----BEGIN RSA PRIVATE KEY-----
          MIIEpAIBAAKCAQEA0LJAufQglnu6jq5RPL+5o3dc/mowFH4NlFyQ55I/4xc9msqn
          ...
        ```

4. Run the Playbook:
    ```bash
    ansible-playbook -i hosts.yml playbook-vault.yml
    ```

5. **Save the `vault_secrets.json` file** and use the root token as the `VAULT_TOKEN` variable in your Nafigos `config.yml`
