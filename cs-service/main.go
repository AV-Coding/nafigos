package main

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/cs-service/cs"

	"github.com/nats-io/stan.go"
)

const (
	defaultClusterID   = "nafigos-cluster"
	defaultClientID    = "cs"
	defaultConnAddress = "nats://localhost:4222"
)

var natsInfo map[string]string

func init() {
	t := time.Now()
	clientID := fmt.Sprintf("%s-%d", defaultClientID, t.Unix())
	natsInfo = common.GetNATSInfo(defaultClusterID, clientID, defaultConnAddress)
}

func main() {
	monitoringSubjects := map[string]stan.MsgHandler{
		"WorkflowDefinition.Create":   cs.WorkflowDefinitionCreate,
		"WorkflowDefinition.Finished": cs.WorkflowDefinitionFinished,
	}
	var wg sync.WaitGroup
	wg.Add(1 + len(monitoringSubjects))
	for subject, handler := range monitoringSubjects {
		go common.StanSubscriber(subject, handler, natsInfo, &wg)
	}
	wg.Wait()
}
