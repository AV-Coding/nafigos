package cs

import (
	"encoding/json"
	"fmt"
	"log"
	"os/exec"

	"github.com/nats-io/stan.go"
	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/workflow-definition-service/workflowdefinition"
)

const (
	defaultClusterID   = "nafigos-cluster"
	defaultClientID    = "workflow-definition"
	defaultConnAddress = "nats://localhost:4222"
)

//Error ...
type Error struct {
}

//Outer ...
type Outer struct {
	Key            Error       `json:"error"`
	Key2           common.User `json:"user"`
	Branch         string      `json:"branch"`
	ClusterID      string      `json:"cluster_id"`
	BuildID        string      `json:"build_id"`
	CommitMsg      string      `json:"commit_msg"`
	GitSecretID    string      `json:"git_secret_id"`
	RegistrySecret string      `json:"registry_secret_id"`
	RunID          string      `json:"run_id"`
	URL            string      `json:"url"`
	WorkflowID     string      `json:"workflow_id"`
}

// Data struct is used to parse through PublishRequest result
type Data struct {
	Datas []workflowdefinition.WorkflowDefinition `json:"data"`
}

// WorkflowDefinitionCreate logs when a WorkflowDefinition is created
func WorkflowDefinitionCreate(msg *stan.Msg) {
	msg.Ack()
	log.Printf("Message on subject WorkflowDefinitionCreate %s for container security:\n", msg)
}

// WorkflowDefinitionFinished ...
func WorkflowDefinitionFinished(msg *stan.Msg) {
	msg.Ack()

	reqBytes, err := common.GetRequestFromCloudEvent(msg.Data)
	if err != nil {
		log.Println("Error getting request from cloud event:", err)
		return
	}

	natsInfo := common.GetNATSInfo(defaultClusterID, defaultClientID, defaultConnAddress)
	var WorkflowDef Outer
	json.Unmarshal(reqBytes, &WorkflowDef)
	fmt.Printf("%+v\n", WorkflowDef)

	var users common.User
	users.Username = WorkflowDef.Key2.Username
	var request workflowdefinition.Request

	request.SetWorkflowID(WorkflowDef.WorkflowID)
	request.SetUser(users)

	result, err := common.PublishRequest(&request, "WorkflowDefinition.GetForUser", "Container Security", natsInfo["address"])
	if err != nil {
		log.Println("Error getting WorkflowDefinition:", err)
		return
	}

	var wfdDatas Data
	//var wfdDatas map[string]interface{}

	json.Unmarshal(result, &wfdDatas)
	//json.MarshallIndent(wfdDatas, "", "\t")
	//fmt.Printf("%+v\n", wfdDatas.Datas[0].Build)
	WFDLength := len(wfdDatas.Datas)
	CurrentWFDIndex := WFDLength - 1
	ImageLength := len(wfdDatas.Datas[CurrentWFDIndex].Build)
	for i := 0; i < ImageLength; i++ {
		fmt.Printf("%+v\n", wfdDatas.Datas[CurrentWFDIndex].Build[i])
		cmd := exec.Command("anchore-cli", "--url", "http://anchore-anchore-engine-api.default.svc.cluster.local:8228/v1/", "--u", "admin", "--p", "foobar", "image", "add", wfdDatas.Datas[CurrentWFDIndex].Build[i].Image)
		if err != nil {
			log.Fatal(err)
		}
		err = cmd.Run()
		if err != nil {
			log.Fatal(err)
		}
	}
}
