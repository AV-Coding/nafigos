package util

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/base64"
	"testing"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/stretchr/testify/assert"
)

func TestCreateCertificateRequest(t *testing.T) {
	key, err := rsa.GenerateKey(rand.Reader, 4096)
	assert.NoError(t, err)
	cr, err := createCertificateRequest("username", key)
	assert.NoError(t, err)
	assert.Contains(t, string(cr), "-----BEGIN CERTIFICATE REQUEST-----")
	assert.Contains(t, string(cr), "-----END CERTIFICATE REQUEST-----")
}

func TestCreateK8sCSR(t *testing.T) {
	csr := createK8sCSR("username", []byte("CERTIFICATE REQUEST"))
	assert.Equal(t, "username", csr.Name)
	assert.Equal(t, []string{"system:authenticated"}, csr.Spec.Groups)
	assert.Equal(t, []byte("CERTIFICATE REQUEST"), csr.Spec.Request)
	assert.Equal(t, "username", csr.Name)
}

func TestCreateK8sCSRApproval(t *testing.T) {
	approval := createK8sCSRApproval("username")
	assert.Equal(t, "username", approval.Name)
	assert.Len(t, approval.Status.Conditions, 1)
	assert.Equal(t, "ApprovedByMyPolicy", approval.Status.Conditions[0].Reason)
}

func TestCreateuserClusterRoleBinding(t *testing.T) {
	rb := createUserClusterRoleBinding("username", "roleRefName")
	assert.Equal(t, "username-roleRefName", rb.Name)
	assert.Equal(t, "username", rb.Namespace)
	assert.Equal(t, "roleRefName", rb.RoleRef.Name)
	assert.Len(t, rb.Subjects, 1)
}

func TestCreateUserConfig(t *testing.T) {
	kubeconfig := base64.StdEncoding.EncodeToString([]byte(`apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: Q0FEYXRhCg==
    server: https://127.0.0.1:32771
  name: kind-user-cluster
contexts:
- context:
    cluster: kind-user-cluster
    user: kind-user-cluster
  name: kind-user-cluster
current-context: kind-user-cluster
kind: Config
preferences: {}
users:
- name: kind-user-cluster
  user:
    client-certificate-data: Q2VydERhdGEK
    client-key-data: S2V5RGF0YQo=`))
	key, err := rsa.GenerateKey(rand.Reader, 4096)
	assert.NoError(t, err)
	userConfig, err := createUserConfig(kubeconfig, []byte("bmV3X2NlcnRkYXRhCg=="), key, "username")
	assert.NoError(t, err)
	assert.Equal(t, "cyverse-user-cluster", userConfig.Clusters[0].Name)
}

func TestCreateUserConfigErrorDecodingAdminConfig(t *testing.T) {
	kubeconfig := `apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: Q0FEYXRhCg==
    server: https://127.0.0.1:32771
  name: kind-user-cluster
contexts:
- context:
    cluster: kind-user-cluster
    user: kind-user-cluster
  name: kind-user-cluster
current-context: kind-user-cluster
kind: Config
preferences: {}
users:
- name: kind-user-cluster
  user:
    client-certificate-data: Q2VydERhdGEK
    client-key-data: S2V5RGF0YQo=`
	key, err := rsa.GenerateKey(rand.Reader, 4096)
	assert.NoError(t, err)
	_, err = createUserConfig(kubeconfig, []byte("bmV3X2NlcnRkYXRhCg=="), key, "username")
	assert.Error(t, err)
}

func TestCreateUserConfigErrorUnmarshalingAdminConfig(t *testing.T) {
	kubeconfig := base64.StdEncoding.EncodeToString([]byte("this ain't yaml"))
	key, err := rsa.GenerateKey(rand.Reader, 4096)
	assert.NoError(t, err)
	_, err = createUserConfig(kubeconfig, []byte("bmV3X2NlcnRkYXRhCg=="), key, "username")
	assert.Error(t, err)
}

func TestCreateNewUserClusterConfig(t *testing.T) {
	clientsets, err := common.NewTestK8sClientsets()
	assert.NoError(t, err)

	adminKubeconfig = base64.StdEncoding.EncodeToString([]byte(`apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: Q0FEYXRhCg==
    server: https://127.0.0.1:32771
  name: kind-user-cluster
contexts:
- context:
    cluster: kind-user-cluster
    user: kind-user-cluster
  name: kind-user-cluster
current-context: kind-user-cluster
kind: Config
preferences: {}
users:
- name: kind-user-cluster
  user:
    client-certificate-data: Q2VydERhdGEK
    client-key-data: S2V5RGF0YQo=`))

	_, err = createNewUserClusterConfig("username", clientsets)
	assert.NoError(t, err)
}
