package util

import (
	"fmt"
	"os"

	"gitlab.com/cyverse/nafigos/common"

	apiauth "istio.io/api/authentication/v1alpha1"
	api "istio.io/api/networking/v1beta1"
	apisec "istio.io/api/security/v1beta1"
	auth "istio.io/client-go/pkg/apis/authentication/v1alpha1"
	istio "istio.io/client-go/pkg/apis/networking/v1beta1"
	security "istio.io/client-go/pkg/apis/security/v1beta1"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	defaultAPIHost = "ca.cyverse.org"
)

var (
	nafigosAPIHost string
)

func init() {
	nafigosAPIHost = os.Getenv("NAFIGOS_API_HOST")
	if len(nafigosAPIHost) == 0 {
		nafigosAPIHost = defaultAPIHost
	}
}

func createIstioIngressService(name string, ports []core.ServicePort) *core.Service {
	return &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name:      name,
			Namespace: "istio-system",
			Labels: map[string]string{
				"app":                         "istio-ingressgateway",
				"istio":                       "ingressgateway",
				"operator.istio.io/component": "IngressGateways",
				"operator.istio.io/managed":   "Reconcile",
				"release":                     "istio",
			},
		},
		Spec: core.ServiceSpec{
			Ports: ports,
			Selector: map[string]string{
				"app":   "istio-ingressgateway",
				"istio": "ingressgateway",
			},
			Type: core.ServiceTypeLoadBalancer,
		},
	}
}

// createDestinationRule creates a new Istio DestinationRule to route to a K8s
// Service based on app name and version
func createDestinationRule(name, id, version string) *istio.DestinationRule {
	return &istio.DestinationRule{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":        name,
				"version":    version,
				"nafigos_id": id,
			},
		},
		Spec: api.DestinationRule{
			Host: name,
			Subsets: []*api.Subset{
				{
					Name: version,
					Labels: map[string]string{
						"app":        name,
						"version":    version,
						"nafigos_id": id,
					},
				},
			},
		},
	}
}

// createVirtualService creates an Istio VirtualService to control routing
// from an endpoint like "/user/workflow" to the correct K8s Service
func createVirtualService(name, host, version, uid string, httpPorts []core.ServicePort, tcpPorts []core.ServicePort) *istio.VirtualService {
	// Create HTTP routes for each port
	var routes []*api.HTTPRoute
	for _, p := range httpPorts {
		// If there is only one port in the slice, do not use port mapping
		path := fmt.Sprintf("/%s:%d", uid, p.Port)
		routes = append(routes, &api.HTTPRoute{
			Match: []*api.HTTPMatchRequest{
				{
					Uri: &api.StringMatch{
						MatchType: &api.StringMatch_Prefix{
							Prefix: path + "/",
						},
					},
				},
				{
					Uri: &api.StringMatch{
						MatchType: &api.StringMatch_Prefix{
							Prefix: path,
						},
					},
				},
			},
			Rewrite: &api.HTTPRewrite{Uri: "/"},
			Route: []*api.HTTPRouteDestination{
				{
					Destination: &api.Destination{
						Host:   name,
						Subset: version,
						Port:   &api.PortSelector{Number: uint32(p.Port)},
					},
				},
			},
		})
	}

	var tcpRoutes []*api.TCPRoute
	for _, p := range tcpPorts {
		tcpRoutes = append(tcpRoutes, &api.TCPRoute{
			Match: []*api.L4MatchAttributes{
				{Port: uint32(p.Port)},
			},
			Route: []*api.RouteDestination{
				{
					Destination: &api.Destination{
						Host:   name,
						Subset: version,
						Port:   &api.PortSelector{Number: uint32(p.Port)},
					},
				},
			},
		})
	}

	// Now create and return VirtualService
	return &istio.VirtualService{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":        name,
				"version":    version,
				"nafigos_id": uid,
			},
		},
		Spec: api.VirtualService{
			Gateways: []string{"nafigos-gateway", name},
			Hosts:    []string{host},
			Http:     routes,
			Tcp:      tcpRoutes,
		},
	}
}

// createVirtualServiceNafigosRoute creates an Istio VirtualService that will
// route to the Nafigos Service Cluster ServiceEntry to enable routing back to
// Nafigos API for scale-from-zero
func createVirtualServiceNafigosRoute(port int) *istio.VirtualService {
	return &istio.VirtualService{
		ObjectMeta: meta.ObjectMeta{
			Name: "nafigos-api-service",
		},
		Spec: api.VirtualService{
			Gateways: []string{"nafigos-gateway", "nafigos-api-proxy"},
			Hosts:    []string{"*"},
			Http: []*api.HTTPRoute{
				{
					Match: []*api.HTTPMatchRequest{
						{
							Uri: &api.StringMatch{
								MatchType: &api.StringMatch_Prefix{
									Prefix: "/",
								},
							},
						},
					},
					Route: []*api.HTTPRouteDestination{
						{
							Destination: &api.Destination{
								Host: nafigosAPIHost,
								Port: &api.PortSelector{Number: uint32(port)},
							},
						},
					},
				},
			},
		},
	}
}

// createServiceEntry creates an Istio ServiceEntry to allow routing to the
// external host that is the Nafigos Service Cluster
func createServiceEntry(address string, port int) *istio.ServiceEntry {
	return &istio.ServiceEntry{
		ObjectMeta: meta.ObjectMeta{
			Name: "nafigos-api-service",
		},
		Spec: api.ServiceEntry{
			Hosts:     []string{nafigosAPIHost},
			Addresses: []string{address + "/32"},
			Ports: []*api.Port{
				{
					Number:   uint32(port),
					Name:     "tls",
					Protocol: "TLS",
				},
			},
			Location:   api.ServiceEntry_MESH_EXTERNAL,
			Resolution: api.ServiceEntry_STATIC,
			Endpoints: []*api.WorkloadEntry{
				{
					Address: address,
				},
			},
		},
	}
}

// createNafigosGateway creates a simple Istio Gateway for Nafigos routing
func createNafigosGateway() *istio.Gateway {
	return &istio.Gateway{
		ObjectMeta: meta.ObjectMeta{
			Name: "nafigos-gateway",
		},
		Spec: api.Gateway{
			Selector: map[string]string{"istio": "ingressgateway"},
			Servers: []*api.Server{
				{
					Hosts: []string{"*"},
					Port: &api.Port{
						Number:   uint32(80),
						Name:     "http",
						Protocol: "HTTP",
					},
				},
			},
		},
	}
}

// createTCPGateway ...
func createTCPGateway(name string, ports []core.ServicePort) *istio.Gateway {
	var servers []*api.Server
	for _, p := range ports {
		servers = append(servers, &api.Server{
			Hosts: []string{"*"},
			Port: &api.Port{
				Number:   uint32(p.Port),
				Name:     p.Name,
				Protocol: "TCP",
			},
		})
	}
	return &istio.Gateway{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
		},
		Spec: api.Gateway{
			Selector: map[string]string{"istio": "ingressgateway"},
			Servers:  servers,
		},
	}
}

// createProxyGateway creates an Istio Gateway for routing back to the Nafigos
// Service Cluster
func createProxyGateway(port int) *istio.Gateway {
	return &istio.Gateway{
		ObjectMeta: meta.ObjectMeta{
			Name: "nafigos-api-service",
		},
		Spec: api.Gateway{
			Selector: map[string]string{"istio": "ingressgateway"},
			Servers: []*api.Server{
				{
					Hosts: []string{nafigosAPIHost},
					Port: &api.Port{
						Number:   uint32(port),
						Name:     "tls",
						Protocol: "TLS",
					},
					Tls: &api.ServerTLSSettings{
						Mode: api.ServerTLSSettings_PASSTHROUGH,
					},
				},
			},
		},
	}
}

// createAuthorizationPolicy creates an Istio AuthorizationPolicy that requires
// a JWT with 'preferred_username' claim matching the URI prefix so that only "UserA"
// can access things running on "/UserA/*" endpoints
func createAuthorizationPolicy(username, host string) *security.AuthorizationPolicy {
	return &security.AuthorizationPolicy{
		ObjectMeta: meta.ObjectMeta{
			Name: username,
		},
		Spec: apisec.AuthorizationPolicy{
			Rules: []*apisec.Rule{
				{
					To: []*apisec.Rule_To{
						{
							Operation: &apisec.Operation{
								Paths: []string{"/*"},
								Hosts: []string{host},
							},
						},
					},
					When: []*apisec.Condition{
						{
							Key:    "request.auth.claims[preferred_username]",
							Values: []string{username},
						},
					},
				},
			},
		},
	}
}

func createAuthorizationPolicyExclude(name, version, host, runID string) *security.AuthorizationPolicy {
	return &security.AuthorizationPolicy{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"nafigos_id": runID,
			},
		},
		Spec: apisec.AuthorizationPolicy{
			Rules: []*apisec.Rule{
				{
					To: []*apisec.Rule_To{
						{
							Operation: &apisec.Operation{
								Hosts: []string{host},
								Paths: []string{fmt.Sprintf("/%s*", runID)},
							},
						},
					},
				},
			},
		},
	}
}

// createKeycloakPolicy creates an Istio Policy requiring a valid JWT for the
// Keycloak server
func createKeycloakPolicy() *auth.Policy {
	keycloakURL := os.Getenv("KEYCLOAK_URL")
	if len(keycloakURL) == 0 {
		return nil
	}
	return &auth.Policy{
		ObjectMeta: meta.ObjectMeta{Name: "default"},
		Spec: apiauth.Policy{
			Origins: []*apiauth.OriginAuthenticationMethod{
				{
					Jwt: &apiauth.Jwt{
						Issuer:     keycloakURL,
						JwtHeaders: []string{"Authorization"},
						Audiences:  []string{"nafigos-client"},
						JwksUri:    keycloakURL + "/protocol/openid-connect/certs",
					},
				},
			},
			PrincipalBinding: apiauth.PrincipalBinding_USE_ORIGIN,
		},
	}
}

func createKeycloakPolicyExclude(name, version, uid string) *auth.Policy {
	keycloakURL := os.Getenv("KEYCLOAK_URL")
	if len(keycloakURL) == 0 {
		return nil
	}
	return &auth.Policy{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"nafigos_id": uid,
			},
		},
		Spec: apiauth.Policy{
			Targets: []*apiauth.TargetSelector{
				{Name: "istio-ingressgateway"},
			},
			Origins: []*apiauth.OriginAuthenticationMethod{
				{
					Jwt: &apiauth.Jwt{
						Issuer:     keycloakURL,
						JwtHeaders: []string{"Authorization"},
						Audiences:  []string{"nafigos-client"},
						JwksUri:    keycloakURL + "/protocol/openid-connect/certs",
						TriggerRules: []*apiauth.Jwt_TriggerRule{
							{
								ExcludedPaths: []*apiauth.StringMatch{
									{
										MatchType: &apiauth.StringMatch_Prefix{
											Prefix: fmt.Sprintf("/%s", uid),
										},
									},
								},
							},
						},
					},
				},
			},
			PrincipalBinding: apiauth.PrincipalBinding_USE_ORIGIN,
		},
	}
}

// getIngressInfo accesses the cluster that this is running on in order to get the
// Host and Port used to access the Istio Ingress Gateway
func getIngressInfo() (address string, port int, err error) {
	clientsets, err := common.NewServiceClusterK8sClientsets()
	if err != nil {
		return
	}
	podList, err := clientsets.ListPods(meta.ListOptions{LabelSelector: "istio=ingressgateway"}, "istio-system")
	if err != nil {
		return
	}
	pod := podList.Items[0]
	address = pod.Status.HostIP

	service, err := clientsets.GetService("istio-ingressgateway", "istio-system")
	if err != nil {
		return
	}
	for _, p := range service.Spec.Ports {
		if p.Name == "http2" {
			port = int(p.NodePort)
			break
		}
	}
	return
}

// prepareAPIServiceProxy creates the necessary Gateways, VirtualService, and ServiceEntry
// to the User cluster in order to proxy requests to non-running services back to the
// Nafigos API
func prepareAPIServiceProxy(clientsets *common.K8sClientsets, namespace string) (err error) {
	address, port, err := getIngressInfo()
	if err != nil {
		return
	}

	// Create a standard gateway
	nafigosGateway := createNafigosGateway()
	_, err = clientsets.CreateGateway(nafigosGateway, namespace)
	if err != nil && err.Error() != `gateways.networking.istio.io "nafigos-gateway" already exists` {
		return
	}
	// Create a Gateway to proxy back to Service Cluster
	proxyGateway := createProxyGateway(port)
	_, err = clientsets.CreateGateway(proxyGateway, namespace)
	if err != nil && err.Error() != `gateways.networking.istio.io "nafigos-api-service" already exists` {
		return
	}
	// Create a ServiceEntry representing the external API Service
	serviceEntry := createServiceEntry(address, port)
	_, err = clientsets.CreateServiceEntry(serviceEntry, namespace)
	if err != nil && err.Error() != `serviceentries.networking.istio.io "nafigos-api-service" already exists` {
		return
	}
	// Create a VirtualService defining path prefix to use for API Service
	virtualService := createVirtualServiceNafigosRoute(port)
	_, err = clientsets.CreateVirtualService(virtualService, namespace)
	if err != nil && err.Error() != `virtualservices.networking.istio.io "nafigos-api-service" already exists` {
		return
	}
	return nil
}
